#ifndef EDIT_PROD
#define EDIT_PROD

#include <QWidget>
#include <QSqlRelationalTableModel>

namespace Ui {
    class edit_prod;
}


/**
  * @class edit_prod
  * ��������� ���� ��������� ��������� ������. ����������� �� ������ <b>QWidget.</b>
*/


class edit_prod : public QWidget
{
    Q_OBJECT

public:
    explicit edit_prod(QWidget *parent = 0);
    ~edit_prod();

private:
    Ui::edit_prod *ui;

    QSqlRelationalTableModel *view_sklad;
    QSqlRelationalTableModel *sklad;
    QSqlRelationalTableModel *platezh;
    QSqlRelationalTableModel *sphaktura;
    QSqlRelationalTableModel *sphaktura_tovar;

void fif();
void fef();

private slots:
    void on_prodat_tovar_clicked();
};

#endif // EDIT_PROD
