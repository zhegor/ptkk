/********************************************************************************
** Form generated from reading UI file 'edit_clients.ui'
**
** Created: Tue 16. Jun 16:31:10 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_CLIENTS_H
#define UI_EDIT_CLIENTS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_clients
{
public:
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *client_name_line;
    QLineEdit *client_email_line;
    QLineEdit *client_inn_line;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *client_adress_line;
    QLineEdit *client_phone_line;
    QLineEdit *client_kpp_line;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_7;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_6;
    QComboBox *bank_combo;
    QLineEdit *client_schet_line;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *add_client;
    QPushButton *delete_client;
    QTableView *tableView;

    void setupUi(QWidget *edit_clients)
    {
        if (edit_clients->objectName().isEmpty())
            edit_clients->setObjectName(QString::fromUtf8("edit_clients"));
        edit_clients->resize(519, 420);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/client.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_clients->setWindowIcon(icon);
        verticalLayout_8 = new QVBoxLayout(edit_clients);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        groupBox = new QGroupBox(edit_clients);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"	font: 10pt \"MS Shell Dlg 2\";\n"
"}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_3->addWidget(label);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_3->addWidget(label_3);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_3->addWidget(label_5);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        client_name_line = new QLineEdit(groupBox);
        client_name_line->setObjectName(QString::fromUtf8("client_name_line"));

        verticalLayout_2->addWidget(client_name_line);

        client_email_line = new QLineEdit(groupBox);
        client_email_line->setObjectName(QString::fromUtf8("client_email_line"));

        verticalLayout_2->addWidget(client_email_line);

        client_inn_line = new QLineEdit(groupBox);
        client_inn_line->setObjectName(QString::fromUtf8("client_inn_line"));

        verticalLayout_2->addWidget(client_inn_line);


        horizontalLayout->addLayout(verticalLayout_2);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_4->addWidget(label_2);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_4->addWidget(label_4);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_4->addWidget(label_6);


        horizontalLayout_2->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        client_adress_line = new QLineEdit(groupBox);
        client_adress_line->setObjectName(QString::fromUtf8("client_adress_line"));

        verticalLayout_5->addWidget(client_adress_line);

        client_phone_line = new QLineEdit(groupBox);
        client_phone_line->setObjectName(QString::fromUtf8("client_phone_line"));

        verticalLayout_5->addWidget(client_phone_line);

        client_kpp_line = new QLineEdit(groupBox);
        client_kpp_line->setObjectName(QString::fromUtf8("client_kpp_line"));

        verticalLayout_5->addWidget(client_kpp_line);


        horizontalLayout_2->addLayout(verticalLayout_5);


        horizontalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_7->addWidget(label_7);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_7->addWidget(label_8);


        horizontalLayout_5->addLayout(verticalLayout_7);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        bank_combo = new QComboBox(groupBox);
        bank_combo->setObjectName(QString::fromUtf8("bank_combo"));

        verticalLayout_6->addWidget(bank_combo);

        client_schet_line = new QLineEdit(groupBox);
        client_schet_line->setObjectName(QString::fromUtf8("client_schet_line"));

        verticalLayout_6->addWidget(client_schet_line);


        horizontalLayout_5->addLayout(verticalLayout_6);


        horizontalLayout_6->addLayout(horizontalLayout_5);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        add_client = new QPushButton(groupBox);
        add_client->setObjectName(QString::fromUtf8("add_client"));
        add_client->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_13->addWidget(add_client);

        delete_client = new QPushButton(groupBox);
        delete_client->setObjectName(QString::fromUtf8("delete_client"));
        delete_client->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_13->addWidget(delete_client);


        verticalLayout->addLayout(horizontalLayout_13);


        verticalLayout_8->addWidget(groupBox);

        tableView = new QTableView(edit_clients);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_8->addWidget(tableView);

        groupBox->raise();
        tableView->raise();
        label->raise();
        label->raise();
        label_3->raise();
        label_5->raise();
        client_name_line->raise();
        client_email_line->raise();
        client_inn_line->raise();
        label_2->raise();
        label_4->raise();
        label_6->raise();
        client_adress_line->raise();
        client_phone_line->raise();
        client_kpp_line->raise();
        label_7->raise();
        bank_combo->raise();
        label_8->raise();
        client_schet_line->raise();
        client_schet_line->raise();
        bank_combo->raise();

        retranslateUi(edit_clients);

        QMetaObject::connectSlotsByName(edit_clients);
    } // setupUi

    void retranslateUi(QWidget *edit_clients)
    {
        edit_clients->setWindowTitle(QString());
        groupBox->setTitle(QApplication::translate("edit_clients", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\272\320\273\320\270\320\265\320\275\321\202\320\260", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_clients", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("edit_clients", "E-Mail", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("edit_clients", "\320\230\320\235\320\235", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_clients", "\320\220\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_clients", "\320\232\320\276\320\275\321\202\320\260\320\272\321\202\320\275\321\213\320\271 \321\202\320\265\320\273\320\265\321\204\320\276\320\275", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("edit_clients", "\320\232\320\237\320\237", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("edit_clients", "\320\221\320\260\320\275\320\272", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("edit_clients", "\320\235\320\276\320\274\320\265\321\200 \321\201\321\207\320\265\321\202\320\260", 0, QApplication::UnicodeUTF8));
        add_client->setText(QApplication::translate("edit_clients", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_client->setText(QApplication::translate("edit_clients", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_clients: public Ui_edit_clients {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_CLIENTS_H
