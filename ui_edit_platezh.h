/********************************************************************************
** Form generated from reading UI file 'edit_platezh.ui'
**
** Created: Tue 16. Jun 16:35:37 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_PLATEZH_H
#define UI_EDIT_PLATEZH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTableView>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_platezh
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTableView *tableView;
    QHBoxLayout *horizontalLayout;
    QToolButton *toolButton;
    QToolButton *print_doc;
    QToolButton *show_doc;

    void setupUi(QWidget *edit_platezh)
    {
        if (edit_platezh->objectName().isEmpty())
            edit_platezh->setObjectName(QString::fromUtf8("edit_platezh"));
        edit_platezh->resize(526, 526);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/doc.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_platezh->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(edit_platezh);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(edit_platezh);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(label);

        tableView = new QTableView(edit_platezh);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout->addWidget(tableView);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        toolButton = new QToolButton(edit_platezh);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(toolButton);

        print_doc = new QToolButton(edit_platezh);
        print_doc->setObjectName(QString::fromUtf8("print_doc"));
        print_doc->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(print_doc);

        show_doc = new QToolButton(edit_platezh);
        show_doc->setObjectName(QString::fromUtf8("show_doc"));
        show_doc->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(show_doc);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(edit_platezh);

        QMetaObject::connectSlotsByName(edit_platezh);
    } // setupUi

    void retranslateUi(QWidget *edit_platezh)
    {
        edit_platezh->setWindowTitle(QApplication::translate("edit_platezh", "\320\237\320\273\320\260\321\202\320\265\320\266\320\275\321\213\320\265 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_platezh", "\320\237\320\273\320\260\321\202\320\265\320\266\320\275\321\213\320\265 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("edit_platezh", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
        print_doc->setText(QApplication::translate("edit_platezh", "\320\235\320\260\320\277\320\265\321\207\320\260\321\202\320\260\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
        show_doc->setText(QApplication::translate("edit_platezh", "\320\237\321\200\320\276\321\201\320\274\320\276\321\202\321\200\320\265\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_platezh: public Ui_edit_platezh {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_PLATEZH_H
