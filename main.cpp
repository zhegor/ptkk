#include "logindialog.h"

#include <QtGui/QApplication>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QTextCodec>
#include <iostream>
#include <cstdio>



/**
 * @mainpage ���������� "���"
 * "���" - ����������, ������� ��������� ����� ���� � �������� ���������� � ������ � ��� "���������� ������������ ��������"
 *
 *@image html img1.PNG
 *
 *  ����������� ����������:
 * - �������� ���������� �� ��������� �������
 * - ����������� ������� ������������ ������, ��������, �����������, ������ � �.�.
 * - ����������� �������� ������ �� ����������� � ��� �������
 * - ����������� ���������� � ������ ���������������� ����������
 *
 * @author ��������� �.�.
 * @author 2015
 */

bool connectDatabase()
{

QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
   db.setHostName("localhost");
   db.setPort(3307);
   db.setDatabaseName("ptk_database");
   db.setUserName("root");
   db.setPassword("usbw");
   return db.open();
}

int main(int argc, char *argv[])
{

    QTextCodec* codec = QTextCodec::codecForName("Windows-1251");
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForTr(codec);


    QApplication a(argc, argv);


  if (!connectDatabase())
    {
              //� ������ ���������� ����������� � �� ������� ��������� �� ������
      QMessageBox::critical(0, "��� ����������", "������ ����������� � ���� ������");
              return 1;
          }

    loginDialog log;
    log.show();

    return a.exec();
}
