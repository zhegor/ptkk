#include "edit_form.h"
#include "ui_edit_form.h"

#include <QDesktopWidget>

edit_form::edit_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_form)
{
    ui->setupUi(this);

    mapper = new QDataWidgetMapper(this);
    mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    //������� ���� � ����� ������
    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - (this->width()/2));
    center.setY(center.y() - (this->height()/2));
    move(center);
}

edit_form::~edit_form()
{
    delete ui;
}

void edit_form::setModel(QAbstractItemModel *model)
{
    mapper->setModel(model);
    mapper->addMapping(ui->line_name,1);
}

void edit_form::on_apply_clicked()
{
   mapper->submit();
   close();
}

void edit_form::on_cancel_clicked()
{
     close();
}
