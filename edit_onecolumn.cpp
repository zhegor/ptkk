#include "edit_onecolumn.h"
#include "ui_edit_onecolumn.h"

#include <QMessageBox>

edit_onecolumn::edit_onecolumn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_onecolumn)
{
    ui->setupUi(this);


}

edit_onecolumn::~edit_onecolumn()
{
    delete ui;
}

//������� �� ���������
void edit_onecolumn::change_to_categorii()
{
    this->setWindowTitle("�������� ���������");
    ui->groupBox->setTitle("�������� ���������");
    ui->label->setText("������������ ���������");

    //����� ������� categorii
    view_smth = new QSqlTableModel(this);
    view_smth->setTable("categorii");
    view_smth->select();
    ui->tableView->setModel(view_smth);
    view_smth->setHeaderData(0,Qt::Horizontal,"ID");
    view_smth->setHeaderData(1,Qt::Horizontal,"������������");
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
}

//������� �� ������
void edit_onecolumn::change_to_strana()
{
    this->setWindowTitle("���������� �����-��������������");
    ui->groupBox->setTitle("�������� ������");
    ui->label->setText("�������� ������");

    //����� ������� categorii
    view_smth = new QSqlTableModel(this);
    view_smth->setTable("strana_pr");
    view_smth->select();
    ui->tableView->setModel(view_smth);
    view_smth->setHeaderData(0,Qt::Horizontal,"ID");
    view_smth->setHeaderData(1,Qt::Horizontal,"������");
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
}

//������� �� ��������� ������
void edit_onecolumn::change_to_sost()
{
    this->setWindowTitle("���� ��������� ������");
    ui->groupBox->setTitle("�������� ���������");
    ui->label->setText("������������ ���������");

    //����� ������� categorii
    view_smth = new QSqlTableModel(this);
    view_smth->setTable("sost_tovar");
    view_smth->select();
    ui->tableView->setModel(view_smth);
    view_smth->setHeaderData(0,Qt::Horizontal,"ID");
    view_smth->setHeaderData(1,Qt::Horizontal,"��������� ������");
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
}



//�������� ������
void edit_onecolumn::on_add_smth_clicked()
{
    //���� ���� �� ���������, ����� ������
    if(ui->name_line->text().isEmpty())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������, ��������� ������
    else
    {
      int row = 0;
      view_smth->insertRows(row, 1);

      //���������
      QModelIndex category_name = view_smth->index(row, 1);
      view_smth->setData(category_name, ui->name_line->text());

      view_smth->submitAll();
      ui->name_line->setText("");
      ui->tableView->scrollToBottom();

       }
}

//������� ������
void edit_onecolumn::on_delete_smth_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ������?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    view_smth->removeRow(index.row());
}
}

//����� ��������������
void edit_onecolumn::on_tableView_activated(QModelIndex index)
{
    //����� ��������������
    show_edit_form = new edit_form();
    show_edit_form->setParent(this,Qt::Window);
    show_edit_form->setModel(view_smth);

    show_edit_form->mapper->setCurrentModelIndex(index);
    show_edit_form->show();
}
