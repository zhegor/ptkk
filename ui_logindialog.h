/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created: Wed 3. Jun 10:31:50 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_loginDialog
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *p;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLineEdit *loginLine;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *passwordLine;
    QVBoxLayout *verticalLayout;
    QLabel *warningLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *loginDialog)
    {
        if (loginDialog->objectName().isEmpty())
            loginDialog->setObjectName(QString::fromUtf8("loginDialog"));
        loginDialog->resize(319, 394);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        loginDialog->setWindowIcon(icon);
        verticalLayout_3 = new QVBoxLayout(loginDialog);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        p = new QLabel(loginDialog);
        p->setObjectName(QString::fromUtf8("p"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(50);
        sizePolicy.setVerticalStretch(50);
        sizePolicy.setHeightForWidth(p->sizePolicy().hasHeightForWidth());
        p->setSizePolicy(sizePolicy);
        p->setMinimumSize(QSize(299, 267));
        p->setStyleSheet(QString::fromUtf8("background-image: url(:/images/prk_logo.png);"));

        verticalLayout_2->addWidget(p);


        verticalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        label = new QLabel(loginDialog);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        loginLine = new QLineEdit(loginDialog);
        loginLine->setObjectName(QString::fromUtf8("loginLine"));

        horizontalLayout->addWidget(loginLine);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(loginDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        passwordLine = new QLineEdit(loginDialog);
        passwordLine->setObjectName(QString::fromUtf8("passwordLine"));
        passwordLine->setInputMask(QString::fromUtf8(""));
        passwordLine->setFrame(true);
        passwordLine->setEchoMode(QLineEdit::Password);
        passwordLine->setDragEnabled(false);
        passwordLine->setReadOnly(false);

        horizontalLayout_2->addWidget(passwordLine);


        verticalLayout_3->addLayout(horizontalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        warningLabel = new QLabel(loginDialog);
        warningLabel->setObjectName(QString::fromUtf8("warningLabel"));
        warningLabel->setStyleSheet(QString::fromUtf8("font: 75 9pt \"MS Shell Dlg 2\";"));
        warningLabel->setTextFormat(Qt::AutoText);
        warningLabel->setScaledContents(true);
        warningLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(warningLabel);

        buttonBox = new QDialogButtonBox(loginDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);

        verticalLayout->addWidget(buttonBox);


        verticalLayout_3->addLayout(verticalLayout);


        retranslateUi(loginDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), loginDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), loginDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(loginDialog);
    } // setupUi

    void retranslateUi(QDialog *loginDialog)
    {
        loginDialog->setWindowTitle(QApplication::translate("loginDialog", "\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217", 0, QApplication::UnicodeUTF8));
        p->setText(QString());
        label->setText(QApplication::translate("loginDialog", "\320\233\320\276\320\263\320\270\320\275", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        loginLine->setWhatsThis(QApplication::translate("loginDialog", "\320\260\320\277\320\277\320\277\320\260\320\277\320\277\320\260", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        loginLine->setText(QApplication::translate("loginDialog", "admin", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("loginDialog", "\320\237\320\260\321\200\320\276\320\273\321\214", 0, QApplication::UnicodeUTF8));
        passwordLine->setText(QApplication::translate("loginDialog", "admin", 0, QApplication::UnicodeUTF8));
        warningLabel->setText(QApplication::translate("loginDialog", "\320\222\320\262\320\265\320\264\320\265\320\275 \320\275\320\265\320\262\320\265\321\200\320\275\321\213\320\271 \320\273\320\276\320\263\320\270\320\275 \320\270\320\273\320\270 \320\277\320\260\321\200\320\276\320\273\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class loginDialog: public Ui_loginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
