/********************************************************************************
** Form generated from reading UI file 'edit_ptk_info.ui'
**
** Created: Tue 16. Jun 16:50:43 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_PTK_INFO_H
#define UI_EDIT_PTK_INFO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_ptk_info
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer_7;
    QLineEdit *ptk_name;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLineEdit *ptk_adres;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *ptk_email;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *ptk_phone;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *ptk_inn;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_5;
    QLineEdit *ptk_kpp;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QComboBox *ptk_bank;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_6;
    QLineEdit *ptk_schet;
    QPushButton *save_info;

    void setupUi(QWidget *edit_ptk_info)
    {
        if (edit_ptk_info->objectName().isEmpty())
            edit_ptk_info->setObjectName(QString::fromUtf8("edit_ptk_info"));
        edit_ptk_info->resize(328, 285);
        verticalLayout = new QVBoxLayout(edit_ptk_info);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(edit_ptk_info);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        ptk_name = new QLineEdit(edit_ptk_info);
        ptk_name->setObjectName(QString::fromUtf8("ptk_name"));

        horizontalLayout->addWidget(ptk_name);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(edit_ptk_info);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        ptk_adres = new QLineEdit(edit_ptk_info);
        ptk_adres->setObjectName(QString::fromUtf8("ptk_adres"));

        horizontalLayout_2->addWidget(ptk_adres);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(edit_ptk_info);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        ptk_email = new QLineEdit(edit_ptk_info);
        ptk_email->setObjectName(QString::fromUtf8("ptk_email"));

        horizontalLayout_3->addWidget(ptk_email);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(edit_ptk_info);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        ptk_phone = new QLineEdit(edit_ptk_info);
        ptk_phone->setObjectName(QString::fromUtf8("ptk_phone"));

        horizontalLayout_4->addWidget(ptk_phone);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(edit_ptk_info);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_5->addWidget(label_5);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);

        ptk_inn = new QLineEdit(edit_ptk_info);
        ptk_inn->setObjectName(QString::fromUtf8("ptk_inn"));

        horizontalLayout_5->addWidget(ptk_inn);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_6 = new QLabel(edit_ptk_info);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_6->addWidget(label_6);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        ptk_kpp = new QLineEdit(edit_ptk_info);
        ptk_kpp->setObjectName(QString::fromUtf8("ptk_kpp"));

        horizontalLayout_6->addWidget(ptk_kpp);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_7 = new QLabel(edit_ptk_info);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_7->addWidget(label_7);

        ptk_bank = new QComboBox(edit_ptk_info);
        ptk_bank->setObjectName(QString::fromUtf8("ptk_bank"));

        horizontalLayout_7->addWidget(ptk_bank);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_8 = new QLabel(edit_ptk_info);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_8->addWidget(label_8);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_6);

        ptk_schet = new QLineEdit(edit_ptk_info);
        ptk_schet->setObjectName(QString::fromUtf8("ptk_schet"));

        horizontalLayout_8->addWidget(ptk_schet);


        verticalLayout->addLayout(horizontalLayout_8);

        save_info = new QPushButton(edit_ptk_info);
        save_info->setObjectName(QString::fromUtf8("save_info"));
        save_info->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(save_info);


        retranslateUi(edit_ptk_info);

        QMetaObject::connectSlotsByName(edit_ptk_info);
    } // setupUi

    void retranslateUi(QWidget *edit_ptk_info)
    {
        edit_ptk_info->setWindowTitle(QApplication::translate("edit_ptk_info", "\320\230\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217 \320\276 \320\277\321\200\320\265\320\264\320\277\321\200\320\270\321\217\321\202\320\270\320\270", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_ptk_info", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265 \320\277\321\200\320\265\320\264\320\277\321\200\320\270\321\217\321\202\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_ptk_info", "\320\220\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("edit_ptk_info", "E-Mail", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_ptk_info", "\320\242\320\265\320\273\320\265\321\204\320\276\320\275", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("edit_ptk_info", "\320\230\320\235\320\235", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("edit_ptk_info", "\320\232\320\237\320\237", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("edit_ptk_info", "\320\221\320\260\320\275\320\272", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("edit_ptk_info", "\320\241\321\207\320\265\321\202", 0, QApplication::UnicodeUTF8));
        save_info->setText(QApplication::translate("edit_ptk_info", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_ptk_info: public Ui_edit_ptk_info {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_PTK_INFO_H
