#ifndef EDIT_BANKS_H
#define EDIT_BANKS_H

#include <QWidget>
#include <QSqlRelationalTableModel>

/**
  * @namespace ������������ ������������ Ui
  * @class ������������ ������� edit_banks
*/
namespace Ui {
    class edit_banks;
}

/**
  * @class edit_banks
  * ��������� ���� ����������� �������� � �����������. ����������� �� QWidget.
*/
class edit_banks : public QWidget
{
    Q_OBJECT

public:
    explicit edit_banks(QWidget *parent = 0);
    ~edit_banks();

private:
    /**
      *�������������� ����� ��������� ������ ����� edit_banks.
      */
    Ui::edit_banks *ui;

    QSqlRelationalTableModel *view_banks;

private slots:
    /**
      *���� �������������� ������ �������� ������.
      */
    void on_delete_bank_clicked();
    /**
      *���� �������������� ������ �������� ������.
      */
    void on_add_bank_clicked();
};

#endif // EDIT_BANKS_H
