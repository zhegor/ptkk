#include "edit_prod.h"
#include "ui_edit_prod.h"

#include <QSqlQueryModel>
#include <QMessageBox>
#include <QSqlQuery>
#include <QDate>

edit_prod::edit_prod(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_prod)
{
    ui->setupUi(this);

    //����� ������ ��������
    QSqlQueryModel *clients_combo = new QSqlQueryModel;
    clients_combo->setQuery("SELECT client_name, client_id FROM clients");
    ui->client_combo->setModel(clients_combo);

    //����� ����
    QDate date = QDate::currentDate();
    ui->date_line->setText(date.toString("dd.MM.yyyy"));

    //����� ������
    sklad = new QSqlRelationalTableModel();
    sklad->setTable("sklad");

    ui->skladTable->setModel(sklad);

    sklad->setHeaderData(1,Qt::Horizontal, "���������");
    sklad->setHeaderData(2,Qt::Horizontal, "�����");
    sklad->setHeaderData(3,Qt::Horizontal, "������");
    sklad->setHeaderData(4,Qt::Horizontal, "��� ������������");
    sklad->setHeaderData(5,Qt::Horizontal, "���������");
    sklad->setHeaderData(6,Qt::Horizontal, "���������� (��.)");
    sklad->setHeaderData(8,Qt::Horizontal, "���� �� ������� (���.)");

    sklad->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    sklad->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    sklad->setRelation(3,QSqlRelation("strana_pr","strana_id","strana_name"));
    sklad->setRelation(5,QSqlRelation("postavshiki","post_id","post_name"));
    sklad->select();
    ui->skladTable->hideColumn(0);
    ui->skladTable->hideColumn(7);
    ui->skladTable->resizeColumnsToContents();

    //����� ��������
    platezh = new QSqlRelationalTableModel();
    platezh->setTable("platezh_poruch");
    ui->platezhTable->setModel(platezh);
    platezh->setRelation(3,QSqlRelation("clients","client_id","client_name"));
    platezh->select();
    platezh->setHeaderData(1,Qt::Horizontal, "�����");
    platezh->setHeaderData(2,Qt::Horizontal, "����");
    platezh->setHeaderData(3,Qt::Horizontal, "������");
    ui->platezhTable->hideColumn(0);
    ui->platezhTable->resizeColumnsToContents();

    //����� �������
    sphaktura = new QSqlRelationalTableModel();
    sphaktura->setTable("sphaktura");
    ui->sphakturaTable->setModel(sphaktura);
    sphaktura->setRelation(3,QSqlRelation("clients","client_id","client_name"));
    sphaktura->setRelation(4,QSqlRelation("platezh_poruch","platezh_id","platezh_nomer"));
    sphaktura->setHeaderData(1,Qt::Horizontal,"�����");
    sphaktura->setHeaderData(2,Qt::Horizontal,"����");
    sphaktura->setHeaderData(3,Qt::Horizontal,"������");
    sphaktura->setHeaderData(4,Qt::Horizontal,"����� ���������� ���������");
    sphaktura->select();
    ui->sphakturaTable->hideColumn(0);

    ui->sphakturaTable->resizeColumnsToContents();

    //����� ������ �� �������
    sphaktura_tovar = new QSqlRelationalTableModel();
    sphaktura_tovar->setTable("sphaktura_tovar");
    ui->sphaktura_tovarTable->setModel(sphaktura_tovar);
    sphaktura_tovar->setRelation(1,QSqlRelation("sphaktura","sphaktura_id","sphaktura_nomer"));
    sphaktura_tovar->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    sphaktura_tovar->setHeaderData(1,Qt::Horizontal,"����� �����-�������");
    sphaktura_tovar->setHeaderData(2,Qt::Horizontal,"�����");
    sphaktura_tovar->setHeaderData(3,Qt::Horizontal,"����������");
    sphaktura_tovar->setHeaderData(4,Qt::Horizontal,"���� �� ������� (���.)");
    sphaktura_tovar->setHeaderData(5,Qt::Horizontal,"����� (���.)");
    ui->sphaktura_tovarTable->hideColumn(0);
    sphaktura_tovar->select();
        ui->sphaktura_tovarTable->resizeColumnsToContents();


    //������� ����� ��������� � �������
    QSqlQuery nomer_platezh;
    nomer_platezh.prepare("SELECT platezh_nomer FROM platezh_poruch WHERE platezh_id = (SELECT MAX(platezh_id) FROM platezh_poruch)");
    nomer_platezh.exec();
    nomer_platezh.next();

    int nmr_plt = nomer_platezh.value(0).toInt() + 1;

    ui->por_nomer_line->setText(QString::number(nmr_plt));

    QSqlQuery nomer_phak;
    nomer_phak.prepare("SELECT sphaktura_nomer FROM sphaktura WHERE sphaktura_id = (SELECT MAX(sphaktura_id) FROM sphaktura)");
    nomer_phak.exec();
    nomer_phak.next();

    int nmr_fak = nomer_phak.value(0).toInt() + 1;

    ui->phak_nomer_line->setText(QString::number(nmr_fak));

    ui->skladTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->platezhTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->sphakturaTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->sphaktura_tovarTable->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_prod::~edit_prod()
{
    delete ui;
}


void edit_prod::on_prodat_tovar_clicked()
{
   //��� ���� ������ ���� ���������
    if (ui->por_nomer_line->text().isEmpty() || ui->phak_nomer_line->text().isEmpty() ||
        ui->kolvo_line->text().isEmpty() || ui->date_line->text().isEmpty() ||
        !ui->skladTable->currentIndex().isValid())
    {
        QMessageBox::warning(this, "������",
                                        "�� ������ �����, ���� �� ��������� ��� ����. ���������� ��� ���.");
    }

    else
    {
        //����������� id ������
        QVariant product_id;
        QModelIndex product_index;
        product_index = ui->skladTable->model()->index(ui->skladTable->currentIndex().row(), 0);
        product_id = ui->skladTable->model()->data(product_index);

        //���� id ������ � ����������� �������
        QSqlQuery findme_id_pls;
        findme_id_pls.exec("SELECT tovar_id FROM sklad WHERE predmet_id = " + product_id.toString() + "");
        findme_id_pls.next();

        QString tovr_id = findme_id_pls.value(0).toString();


        QSqlQuery query_kol_tovar;
        query_kol_tovar.prepare("SELECT kol_vo FROM sklad WHERE predmet_id = "
                         + product_id.toString() +"");
        query_kol_tovar.exec();
        query_kol_tovar.next();
        int a = query_kol_tovar.value(0).toInt();

        //�������� ���������� ������
        if (a < ui->kolvo_line->text().toInt())
        {
            QMessageBox::warning(this, "������",
                                      "���������� ������������ ������ ��������� ���������� ������ �� ������.\n"
                                      "�� ������: " + QString::number(a) +" ��. \n"
                                      "�� �������: " + ui->kolvo_line->text() + " ��.");
        }

        //���� �� �� ����������
        else
        {
            //��������� ��������

            //����������� id �������
            QModelIndex client_index = ui->client_combo->model()->index(ui->client_combo->currentIndex(),1);
            QString client_str = ui->client_combo->model()->data(client_index).toString();

            QSqlQuery setData_platezh;
            setData_platezh.prepare("INSERT INTO platezh_poruch VALUES (?,?,?,?)");
            setData_platezh.addBindValue("AUTO_INCREMENT");
            setData_platezh.addBindValue(ui->por_nomer_line->text());
            setData_platezh.addBindValue(ui->date_line->text());
            setData_platezh.addBindValue(client_str);
            setData_platezh.exec();

            platezh->select();

            //����������� ����� ��������
            QSqlQuery findme_platezh_id;
            findme_platezh_id.exec("SELECT platezh_id FROM platezh_poruch WHERE platezh_nomer = '" + ui->por_nomer_line->text() + "'");
            findme_platezh_id.next();

            //��������� ����-�������
            QSqlQuery setData_phaktura;
            setData_phaktura.prepare("INSERT INTO sphaktura VALUES (?,?,?,?,?,?)");
            setData_phaktura.addBindValue("AUTO_INCREMENT");
            setData_phaktura.addBindValue(ui->phak_nomer_line->text());
            setData_phaktura.addBindValue(ui->date_line->text());
            setData_phaktura.addBindValue(client_str);
            setData_phaktura.addBindValue(findme_platezh_id.value(0).toString());
            setData_phaktura.addBindValue("0");
            setData_phaktura.exec();

            sphaktura->select();

            //����������� id �������
            QSqlQuery findme_phak_id;
            findme_phak_id.exec("SELECT sphaktura_id FROM sphaktura WHERE sphaktura_nomer = '" + ui->phak_nomer_line->text() +"'");
            findme_phak_id.next();

            //������� ���� ����� ������
            QSqlQuery tsena_kolvo;
            tsena_kolvo.exec("SELECT tsena_itog FROM sklad WHERE predmet_id = '" + product_id.toString() +"'");
            tsena_kolvo.next();

            float tsena_natovar = tsena_kolvo.value(0).toFloat();

            float tsena_summ = tsena_natovar * ui->kolvo_line->text().toFloat();

            float tsena_procent = (tsena_natovar * ui->kolvo_line->text().toFloat()) * 0.18;

            float tsena_itog = tsena_summ + tsena_procent;

            //��������� ������ � ����-�������
            QSqlQuery setData_phakturaTovar;
            setData_phakturaTovar.prepare("INSERT INTO sphaktura_tovar VALUES (?,?,?,?,?,?)");
            setData_phakturaTovar.addBindValue("AUTO_INCREMENT");
            setData_phakturaTovar.addBindValue(findme_phak_id.value(0).toInt());
            setData_phakturaTovar.addBindValue(tovr_id);
            setData_phakturaTovar.addBindValue(ui->kolvo_line->text().toInt());
            setData_phakturaTovar.addBindValue(tsena_natovar);
            setData_phakturaTovar.addBindValue(tsena_itog);
            setData_phakturaTovar.exec();

                       //���������
//            QModelIndex id_fak = sphaktura_tovar->index(row, 1);
//            sphaktura_tovar->setData(id_fak, findme_phak_id.value(0).toInt());

//            QModelIndex id_fak1 = sphaktura_tovar->index(row, 2);
//            sphaktura_tovar->setData(id_fak1, tovr_id);

//            QModelIndex id_fak2 = sphaktura_tovar->index(row, 3);
//            sphaktura_tovar->setData(id_fak2, ui->kolvo_line->text().toInt());

//            QModelIndex id_fak3 = sphaktura_tovar->index(row, 4);
//            sphaktura_tovar->setData(id_fak3, tsena_natovar);

//            QModelIndex id_fak4 = sphaktura_tovar->index(row, 5);
//            sphaktura_tovar->setData(id_fak4, tsena_itog);

//            sphaktura_tovar->submitAll();

//            ui->lineEdit->setText(tovr_id);

            sphaktura_tovar->select();

            ui->platezhTable->resizeColumnsToContents();
            ui->sphakturaTable->resizeColumnsToContents();
            ui->sphaktura_tovarTable->resizeColumnsToContents();


            //������� ����� �� ������
            //�����
                      int kolvo_1;
                      int kolvo_2;

                      QSqlQuery query;
                      query.prepare("SELECT kol_vo FROM sklad WHERE predmet_id = '" +
                                    product_id.toString() + "'" );
                      query.exec();
                      query.next();

                      kolvo_1 = query.value(0).toInt();
                      kolvo_2 = ui->kolvo_line->text().toInt();

                      int kolvo_fin = kolvo_1 - kolvo_2;

                      QString kolvo_fin_str = QString::number(kolvo_fin);

                      QSqlQuery query_add;
                      query_add.prepare("UPDATE sklad SET kol_vo = " + kolvo_fin_str +
                                        " WHERE predmet_id = " + product_id.toString() + "");
                      query_add.exec();


                      //������� ����� �� ������, ���� ���������� ����� ������� ����
                      QSqlQuery query_empty;
                      query_empty.prepare("SELECT kol_vo FROM sklad WHERE predmet_id = '" +
                                          product_id.toString() + "'");

                      query_empty.exec();
                      query_empty.next();

                      int kolvo_itog = query_empty.value(0).toInt();

                      if (kolvo_itog <= 0)
                      {
                         //������ ������ ����, ������� ��
                          QSqlQuery query_delete;
                          query_delete.prepare("DELETE FROM sklad WHERE kol_vo = 0");
                          query_delete.exec();

                          QSqlQuery query_tovar_delete;
                          query_tovar_delete.exec("UPDATE tovar SET sost_id ='3' WHERE predmet_id ='" + product_id.toString() +"'");
                      }

                      sklad->select();

                      int nmr_plt = ui->por_nomer_line->text().toInt() + 1;
                      ui->por_nomer_line->setText(QString::number(nmr_plt));

                      int nmr_phak = ui->phak_nomer_line->text().toInt() + 1;
                      ui->phak_nomer_line->setText(QString::number(nmr_phak));

                      ui->kolvo_line->clear();

        }

    }

}
