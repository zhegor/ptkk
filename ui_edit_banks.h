/********************************************************************************
** Form generated from reading UI file 'edit_banks.ui'
**
** Created: Tue 16. Jun 15:57:31 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_BANKS_H
#define UI_EDIT_BANKS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_banks
{
public:
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_3;
    QLineEdit *bank_name_line;
    QLineEdit *bik_line;
    QLineEdit *korschet_line;
    QLineEdit *adress_line;
    QHBoxLayout *horizontalLayout;
    QPushButton *add_bank;
    QPushButton *delete_bank;
    QTableView *tableView;

    void setupUi(QWidget *edit_banks)
    {
        if (edit_banks->objectName().isEmpty())
            edit_banks->setObjectName(QString::fromUtf8("edit_banks"));
        edit_banks->resize(370, 428);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/bank.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_banks->setWindowIcon(icon);
        verticalLayout_4 = new QVBoxLayout(edit_banks);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(edit_banks);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"	font: 10pt \"MS Shell Dlg 2\";\n"
"}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_2->addWidget(label_4);


        horizontalLayout_6->addLayout(verticalLayout_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        bank_name_line = new QLineEdit(groupBox);
        bank_name_line->setObjectName(QString::fromUtf8("bank_name_line"));

        verticalLayout_3->addWidget(bank_name_line);

        bik_line = new QLineEdit(groupBox);
        bik_line->setObjectName(QString::fromUtf8("bik_line"));

        verticalLayout_3->addWidget(bik_line);

        korschet_line = new QLineEdit(groupBox);
        korschet_line->setObjectName(QString::fromUtf8("korschet_line"));

        verticalLayout_3->addWidget(korschet_line);

        adress_line = new QLineEdit(groupBox);
        adress_line->setObjectName(QString::fromUtf8("adress_line"));

        verticalLayout_3->addWidget(adress_line);


        horizontalLayout_6->addLayout(verticalLayout_3);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        add_bank = new QPushButton(groupBox);
        add_bank->setObjectName(QString::fromUtf8("add_bank"));
        add_bank->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(add_bank);

        delete_bank = new QPushButton(groupBox);
        delete_bank->setObjectName(QString::fromUtf8("delete_bank"));
        delete_bank->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(delete_bank);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_4->addWidget(groupBox);

        tableView = new QTableView(edit_banks);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setProperty("showSortIndicator", QVariant(true));
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_4->addWidget(tableView);

        groupBox->raise();
        tableView->raise();
        bank_name_line->raise();
        bik_line->raise();
        korschet_line->raise();
        label->raise();
        label_2->raise();
        label_3->raise();
        label_4->raise();
        bank_name_line->raise();
        bik_line->raise();
        korschet_line->raise();
        adress_line->raise();

        retranslateUi(edit_banks);

        QMetaObject::connectSlotsByName(edit_banks);
    } // setupUi

    void retranslateUi(QWidget *edit_banks)
    {
        edit_banks->setWindowTitle(QApplication::translate("edit_banks", "\320\241\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272 \320\261\320\260\320\275\320\272\320\276\320\262", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_banks", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\261\320\260\320\275\320\272", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_banks", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265 \320\261\320\260\320\275\320\272\320\260", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_banks", "\320\221\320\230\320\232", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("edit_banks", "\320\232\320\276\321\200\321\200. \321\201\321\207\320\265\321\202", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_banks", "\320\220\320\264\321\200\320\265\321\201", 0, QApplication::UnicodeUTF8));
        bank_name_line->setText(QString());
        bik_line->setText(QString());
        korschet_line->setText(QString());
        adress_line->setText(QString());
        add_bank->setText(QApplication::translate("edit_banks", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_bank->setText(QApplication::translate("edit_banks", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_banks: public Ui_edit_banks {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_BANKS_H
