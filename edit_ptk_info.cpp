#include "edit_ptk_info.h"
#include "ui_edit_ptk_info.h"

#include <QSqlQuery>

edit_ptk_info::edit_ptk_info(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_ptk_info)
{
    ui->setupUi(this);

    banks = new QSqlQueryModel();
    banks->setQuery("SELECT bank_name, bank_id FROM banks");
    ui->ptk_bank->setModel(banks);

    QSqlQuery getme_bankid_pls;
    getme_bankid_pls.exec("SELECT bank_id FROM ptk_info");
    getme_bankid_pls.next();
    QSqlQuery name_bank_pls;
    name_bank_pls.exec("SELECT bank_name FROM banks WHERE bank_id = " + getme_bankid_pls.value(0).toString() + "");
    name_bank_pls.next();

    ui->ptk_bank->setCurrentIndex(ui->ptk_bank->findText(name_bank_pls.value(0).toString()));

    //������� ������ � ���
    //������� ���������� � ���
    QSqlQuery getme_ptk_pls;
    getme_ptk_pls.exec("SELECT ptk_name, ptk_inn FROM ptk_info");
    getme_ptk_pls.next();
    ui->ptk_name->setText(getme_ptk_pls.value(0).toString());

    QSqlQuery getme_inn_pls;
    getme_inn_pls.exec("SELECT ptk_inn FROM ptk_info");
    getme_inn_pls.next();
    ui->ptk_inn->setText(getme_inn_pls.value(0).toString());

    QSqlQuery getme_kpp_pls;
    getme_kpp_pls.exec("SELECT ptk_kpp FROM ptk_info");
    getme_kpp_pls.next();
    ui->ptk_kpp->setText(getme_kpp_pls.value(0).toString());

    QSqlQuery getme_schet_pls;
    getme_schet_pls.exec("SELECT ptk_schet FROM ptk_info");
    getme_schet_pls.next();
    ui->ptk_schet->setText(getme_schet_pls.value(0).toString());

    QSqlQuery getme_adres_pls;
    getme_adres_pls.exec("SELECT ptk_adress FROM ptk_info");
    getme_adres_pls.next();
    ui->ptk_adres->setText(getme_adres_pls.value(0).toString());

    QSqlQuery email;
    email.exec("SELECT ptk_email FROM ptk_info");
    email.next();
    ui->ptk_email->setText(email.value(0).toString());

    QSqlQuery phone;
    phone.exec("SELECT ptk_phone FROM ptk_info");
    phone.next();
    ui->ptk_phone->setText(phone.value(0).toString());
}

edit_ptk_info::~edit_ptk_info()
{
    delete ui;
}

void edit_ptk_info::on_save_info_clicked()
{
    QModelIndex index3 = ui->ptk_bank->model()->index(ui->ptk_bank->currentIndex(),1);
    QString bankid = ui->ptk_bank->model()->data(index3).toString();

    QSqlQuery setData_phaktura;
    setData_phaktura.prepare("UPDATE ptk_info SET "
                             "ptk_name ='" + ui->ptk_name->text() + "'" +
                             ", ptk_adress ='" + ui->ptk_adres->text() + "'" +
                             ", ptk_email ='" + ui->ptk_email->text() + "'" +
                             ", ptk_phone ='" + ui->ptk_phone->text() + "'" +
                             ", ptk_INN = '" + ui->ptk_inn->text() + "'" +
                             ", ptk_KPP = '" + ui->ptk_kpp->text() + "'" +
                             ", bank_id = '" + bankid + "'" +
                             ", ptk_schet = '" + ui->ptk_schet->text() + "'");
    setData_phaktura.exec();
}
