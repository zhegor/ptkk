#ifndef EDIT_PROIZV_H
#define EDIT_PROIZV_H

#include <QWidget>
#include <QSqlRelationalTableModel>

/**
  * @namespace ������������ ������������ Ui
  * @class ������������ ������� edit_proizv
*/
namespace Ui {
    class edit_proizv;
}

/**
  * @class edit_proizv
  * ��������� ���� � ����������� � �������������� ������. ����������� �� QWidget.
*/
class edit_proizv : public QWidget
{
    Q_OBJECT

public:
    explicit edit_proizv(QWidget *parent = 0);
    ~edit_proizv();

private:
    /**
      *�������������� ����� ��������� ������ ����� edit_proizv.
      */
    Ui::edit_proizv *ui;

    QSqlRelationalTableModel *view_proizv;

private slots:
    /**
      *���� �������������� ������ �������� ������.
      */
    void on_delete_2_clicked();
    /**
      *���� �������������� ������ ���������� ������.
      */
    void on_add_clicked();
};

#endif // EDIT_PROIZV_H
