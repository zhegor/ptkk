/********************************************************************************
** Form generated from reading UI file 'edit_prod.ui'
**
** Created: Tue 16. Jun 16:45:59 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_PROD_H
#define UI_EDIT_PROD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_prod
{
public:
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QComboBox *client_combo;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *date_line;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *por_nomer_line;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QLineEdit *phak_nomer_line;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_9;
    QTableView *skladTable;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_10;
    QLineEdit *kolvo_line;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *prodat_tovar;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout;
    QLabel *label_6;
    QTableView *platezhTable;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_7;
    QTableView *sphakturaTable;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QTableView *sphaktura_tovarTable;

    void setupUi(QWidget *edit_prod)
    {
        if (edit_prod->objectName().isEmpty())
            edit_prod->setObjectName(QString::fromUtf8("edit_prod"));
        edit_prod->resize(659, 658);
        verticalLayout_6 = new QVBoxLayout(edit_prod);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox_3 = new QGroupBox(edit_prod);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_5 = new QVBoxLayout(groupBox_3);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        client_combo = new QComboBox(groupBox_3);
        client_combo->setObjectName(QString::fromUtf8("client_combo"));

        horizontalLayout_3->addWidget(client_combo);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        date_line = new QLineEdit(groupBox_3);
        date_line->setObjectName(QString::fromUtf8("date_line"));
        date_line->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_2->addWidget(date_line);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout_5->addLayout(horizontalLayout_2);


        verticalLayout_6->addWidget(groupBox_3);

        groupBox = new QGroupBox(edit_prod);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        por_nomer_line = new QLineEdit(groupBox);
        por_nomer_line->setObjectName(QString::fromUtf8("por_nomer_line"));
        por_nomer_line->setMaximumSize(QSize(60, 16777215));

        horizontalLayout->addWidget(por_nomer_line);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout);


        verticalLayout_6->addWidget(groupBox);

        groupBox_2 = new QGroupBox(edit_prod);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_2);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_5->addWidget(label_4);

        phak_nomer_line = new QLineEdit(groupBox_2);
        phak_nomer_line->setObjectName(QString::fromUtf8("phak_nomer_line"));
        phak_nomer_line->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_5->addWidget(phak_nomer_line);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);


        horizontalLayout_7->addLayout(horizontalLayout_5);


        verticalLayout_6->addWidget(groupBox_2);

        label_9 = new QLabel(edit_prod);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_6->addWidget(label_9);

        skladTable = new QTableView(edit_prod);
        skladTable->setObjectName(QString::fromUtf8("skladTable"));
        skladTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        skladTable->setSortingEnabled(true);

        verticalLayout_6->addWidget(skladTable);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_10 = new QLabel(edit_prod);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_9->addWidget(label_10);

        kolvo_line = new QLineEdit(edit_prod);
        kolvo_line->setObjectName(QString::fromUtf8("kolvo_line"));
        kolvo_line->setMaximumSize(QSize(90, 16777215));

        horizontalLayout_9->addWidget(kolvo_line);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_4);


        verticalLayout_6->addLayout(horizontalLayout_9);

        prodat_tovar = new QPushButton(edit_prod);
        prodat_tovar->setObjectName(QString::fromUtf8("prodat_tovar"));
        prodat_tovar->setStyleSheet(QString::fromUtf8("font: 11pt \"MS Shell Dlg 2\";"));

        verticalLayout_6->addWidget(prodat_tovar);

        line_2 = new QFrame(edit_prod);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_6->addWidget(line_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_6 = new QLabel(edit_prod);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(label_6);

        platezhTable = new QTableView(edit_prod);
        platezhTable->setObjectName(QString::fromUtf8("platezhTable"));
        platezhTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        platezhTable->setSortingEnabled(true);

        verticalLayout->addWidget(platezhTable);


        horizontalLayout_8->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_7 = new QLabel(edit_prod);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_2->addWidget(label_7);

        sphakturaTable = new QTableView(edit_prod);
        sphakturaTable->setObjectName(QString::fromUtf8("sphakturaTable"));
        sphakturaTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        sphakturaTable->setSortingEnabled(true);

        verticalLayout_2->addWidget(sphakturaTable);


        horizontalLayout_8->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_8 = new QLabel(edit_prod);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_3->addWidget(label_8);

        sphaktura_tovarTable = new QTableView(edit_prod);
        sphaktura_tovarTable->setObjectName(QString::fromUtf8("sphaktura_tovarTable"));
        sphaktura_tovarTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        sphaktura_tovarTable->setSortingEnabled(false);

        verticalLayout_3->addWidget(sphaktura_tovarTable);


        horizontalLayout_8->addLayout(verticalLayout_3);


        verticalLayout_6->addLayout(horizontalLayout_8);


        retranslateUi(edit_prod);

        QMetaObject::connectSlotsByName(edit_prod);
    } // setupUi

    void retranslateUi(QWidget *edit_prod)
    {
        edit_prod->setWindowTitle(QApplication::translate("edit_prod", "\320\237\321\200\320\276\320\264\320\260\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QString());
        label_3->setText(QApplication::translate("edit_prod", "\320\232\320\273\320\270\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_prod", "\320\224\320\260\321\202\320\260", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_prod", "\320\237\320\273\320\260\321\202\320\265\320\266\320\275\320\276\320\265 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_prod", "\320\235\320\276\320\274\320\265\321\200 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("edit_prod", "\320\241\321\207\320\265\321\202-\321\204\320\260\320\272\321\202\321\203\321\200\320\260", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_prod", "\320\235\320\276\320\274\320\265\321\200 \321\204\320\260\320\272\321\202\321\203\321\200\321\213", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("edit_prod", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214 \321\202\320\276\320\262\320\260\321\200 \321\201\320\276 \321\201\320\272\320\273\320\260\320\264\320\260", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("edit_prod", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276", 0, QApplication::UnicodeUTF8));
        prodat_tovar->setText(QApplication::translate("edit_prod", "\320\237\321\200\320\276\320\264\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("edit_prod", "\320\237\320\273\320\260\321\202\320\265\320\266\320\275\321\213\320\265 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("edit_prod", "\320\241\321\207\320\265\321\202\320\260-\321\204\320\260\320\272\321\202\321\203\321\200\321\213", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("edit_prod", "\320\241\321\207\320\265\321\202\320\260-\321\204\320\260\320\272\321\202\321\203\321\200\321\213: \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_prod: public Ui_edit_prod {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_PROD_H
