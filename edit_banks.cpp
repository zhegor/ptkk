#include "edit_banks.h"
#include "ui_edit_banks.h"

#include <QMessageBox>

edit_banks::edit_banks(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_banks)
{
    ui->setupUi(this);

    //������� ������ ��� ������ ������
    view_banks = new QSqlRelationalTableModel();
    view_banks->setTable("banks");
    view_banks->select();

    //����������� ������ � ������� �������
    ui->tableView->setModel(view_banks);

    view_banks->setHeaderData(1,Qt::Horizontal,"������������ �����");
    view_banks->setHeaderData(2,Qt::Horizontal, "���");
    view_banks->setHeaderData(3, Qt::Horizontal, "����. ����");
    view_banks->setHeaderData(4, Qt::Horizontal, "�����");
    ui->tableView->hideColumn(0);
    ui->tableView->resizeColumnsToContents();

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_banks::~edit_banks()
{
    delete ui;
}

//�������� ����
void edit_banks::on_add_bank_clicked()
{
    //���� ���� �� ���������, ����� ������
    if(ui->bank_name_line->text().isEmpty() ||
       ui->bik_line->text().isEmpty() ||
       ui->korschet_line->text().isEmpty())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������, ��������� ������
    else
    {
      int row = 0;
      view_banks->insertRows(row, 1);

      //��� �����
      QModelIndex bank_name = view_banks->index(row, 1);
      view_banks->setData(bank_name, ui->bank_name_line->text());
      //���
      QModelIndex bik = view_banks->index(row, 2);
      view_banks->setData(bik, ui->bik_line->text());
      //�������
      QModelIndex korschet = view_banks->index(row, 3);
      view_banks->setData(korschet, ui->korschet_line->text());
      //������
      QModelIndex adress = view_banks->index(row,4);
      view_banks->setData(adress, ui->adress_line->text());

      view_banks->submitAll();
      ui->bank_name_line->setText("");
      ui->bik_line->setText("");
      ui->korschet_line->setText("");
      ui->adress_line->setText("");
      ui->tableView->scrollToBottom();

      }

}


//������� ����
void edit_banks::on_delete_bank_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ����?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    view_banks->removeRow(index.row());
}
}
