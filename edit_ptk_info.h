#ifndef EDIT_PTK_INFO_H
#define EDIT_PTK_INFO_H

#include <QWidget>
#include <QSqlQueryModel>

/**
  * @namespace ������������ ������������ Ui
  * @class ������������ ������� edit_ptk_info
*/
namespace Ui {
    class edit_ptk_info;
}


/**
  * @class edit_ptk_info
  * ��������� ���� � ����������� � �����������. ����������� �� QWidget.
*/
class edit_ptk_info : public QWidget
{
    Q_OBJECT

public:
    explicit edit_ptk_info(QWidget *parent = 0);
    ~edit_ptk_info();

private:
    /**
      *�������������� ����� ��������� ������ ����� edit_ptk_info.
      */
    Ui::edit_ptk_info *ui;

    QSqlQueryModel *banks;

private slots:
    /**
      *���� �������������� ������ ���������� ����������.
      */
    void on_save_info_clicked();
};

#endif // EDIT_PTK_INFO_H
