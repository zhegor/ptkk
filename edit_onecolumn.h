#ifndef EDIT_ONECOLUMN_H
#define EDIT_ONECOLUMN_H

#include <QWidget>
#include <QSqlTableModel>

#include "edit_form.h"

namespace Ui {
    class edit_onecolumn;
}

class edit_onecolumn : public QWidget
{
    Q_OBJECT

public:
    explicit edit_onecolumn(QWidget *parent = 0);
    ~edit_onecolumn();

    void change_to_categorii();
    void change_to_strana();
    void change_to_sost();

private:
    Ui::edit_onecolumn *ui;

    QSqlTableModel *view_smth;

    edit_form *show_edit_form;

private slots:
    void on_tableView_activated(QModelIndex index);
    void on_delete_smth_clicked();
    void on_add_smth_clicked();
};

#endif // EDIT_ONECOLUMN_H
