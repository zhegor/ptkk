/********************************************************************************
** Form generated from reading UI file 'mainwidget.ui'
**
** Created: Sat 25. Apr 15:55:10 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIDGET_H
#define UI_MAINWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWidget
{
public:
    QVBoxLayout *verticalLayout_5;
    QGroupBox *groupBox_tables;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *tovar_Button;
    QToolButton *strana_pr_Button;
    QToolButton *postavshiki_Button;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *categories_Button;
    QToolButton *tovar_sost_Button;
    QGroupBox *groupBox_trade;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *postavki;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *prodazha;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *sklad;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_7;
    QToolButton *logout;

    void setupUi(QWidget *MainWidget)
    {
        if (MainWidget->objectName().isEmpty())
            MainWidget->setObjectName(QString::fromUtf8("MainWidget"));
        MainWidget->setWindowModality(Qt::NonModal);
        MainWidget->resize(422, 269);
        MainWidget->setMouseTracking(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWidget->setWindowIcon(icon);
        verticalLayout_5 = new QVBoxLayout(MainWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        groupBox_tables = new QGroupBox(MainWidget);
        groupBox_tables->setObjectName(QString::fromUtf8("groupBox_tables"));
        verticalLayout_4 = new QVBoxLayout(groupBox_tables);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tovar_Button = new QToolButton(groupBox_tables);
        tovar_Button->setObjectName(QString::fromUtf8("tovar_Button"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tovar_Button->sizePolicy().hasHeightForWidth());
        tovar_Button->setSizePolicy(sizePolicy);
        tovar_Button->setMinimumSize(QSize(0, 25));
        tovar_Button->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(tovar_Button);

        strana_pr_Button = new QToolButton(groupBox_tables);
        strana_pr_Button->setObjectName(QString::fromUtf8("strana_pr_Button"));
        strana_pr_Button->setMinimumSize(QSize(0, 25));
        strana_pr_Button->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(strana_pr_Button);

        postavshiki_Button = new QToolButton(groupBox_tables);
        postavshiki_Button->setObjectName(QString::fromUtf8("postavshiki_Button"));
        postavshiki_Button->setMinimumSize(QSize(0, 25));
        postavshiki_Button->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(postavshiki_Button);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        categories_Button = new QToolButton(groupBox_tables);
        categories_Button->setObjectName(QString::fromUtf8("categories_Button"));
        categories_Button->setMinimumSize(QSize(0, 25));
        categories_Button->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(categories_Button);

        tovar_sost_Button = new QToolButton(groupBox_tables);
        tovar_sost_Button->setObjectName(QString::fromUtf8("tovar_sost_Button"));
        tovar_sost_Button->setMinimumSize(QSize(0, 25));
        tovar_sost_Button->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(tovar_sost_Button);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_4->addLayout(verticalLayout);


        verticalLayout_5->addWidget(groupBox_tables);

        groupBox_trade = new QGroupBox(MainWidget);
        groupBox_trade->setObjectName(QString::fromUtf8("groupBox_trade"));
        verticalLayout_3 = new QVBoxLayout(groupBox_trade);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        postavki = new QPushButton(groupBox_trade);
        postavki->setObjectName(QString::fromUtf8("postavki"));
        postavki->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_3->addWidget(postavki);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        prodazha = new QPushButton(groupBox_trade);
        prodazha->setObjectName(QString::fromUtf8("prodazha"));
        prodazha->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_5->addWidget(prodazha);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_5);


        verticalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        sklad = new QPushButton(groupBox_trade);
        sklad->setObjectName(QString::fromUtf8("sklad"));
        sklad->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_4->addWidget(sklad);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        verticalLayout_3->addLayout(horizontalLayout_4);


        verticalLayout_5->addWidget(groupBox_trade);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_7);

        logout = new QToolButton(MainWidget);
        logout->setObjectName(QString::fromUtf8("logout"));

        horizontalLayout_6->addWidget(logout);


        verticalLayout_5->addLayout(horizontalLayout_6);


        retranslateUi(MainWidget);

        QMetaObject::connectSlotsByName(MainWidget);
    } // setupUi

    void retranslateUi(QWidget *MainWidget)
    {
        MainWidget->setWindowTitle(QApplication::translate("MainWidget", "\320\236\320\236\320\236 \"\320\237\321\200\320\270\320\274\320\276\321\200\321\201\320\272\320\260\321\217 \320\242\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\275\320\260\321\217 \320\232\320\276\320\274\320\277\320\260\320\275\320\270\321\217\"", 0, QApplication::UnicodeUTF8));
        groupBox_tables->setTitle(QApplication::translate("MainWidget", "\320\242\320\260\320\261\320\273\320\270\321\206\321\213-\321\201\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272\320\270", 0, QApplication::UnicodeUTF8));
        tovar_Button->setText(QApplication::translate("MainWidget", "\320\242\320\276\320\262\320\260\321\200\321\213", 0, QApplication::UnicodeUTF8));
        strana_pr_Button->setText(QApplication::translate("MainWidget", "\320\241\321\202\321\200\320\260\320\275\321\213 \320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\320\270", 0, QApplication::UnicodeUTF8));
        postavshiki_Button->setText(QApplication::translate("MainWidget", "\320\237\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272\320\270", 0, QApplication::UnicodeUTF8));
        categories_Button->setText(QApplication::translate("MainWidget", "\320\232\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\270 \321\202\320\276\320\262\320\260\321\200\320\276\320\262", 0, QApplication::UnicodeUTF8));
        tovar_sost_Button->setText(QApplication::translate("MainWidget", "\320\222\320\270\320\264\321\213 \321\201\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\271 \321\202\320\276\320\262\320\260\321\200\320\276\320\262", 0, QApplication::UnicodeUTF8));
        groupBox_trade->setTitle(QApplication::translate("MainWidget", "\320\237\320\276\321\201\321\202\320\260\320\262\320\272\320\260 \320\270 \320\277\321\200\320\276\320\264\320\260\320\266\320\260", 0, QApplication::UnicodeUTF8));
        postavki->setText(QApplication::translate("MainWidget", "\320\237\320\276\321\201\321\202\320\260\320\262\320\272\320\270", 0, QApplication::UnicodeUTF8));
        prodazha->setText(QApplication::translate("MainWidget", "\320\237\321\200\320\276\320\264\320\260\320\266\320\260", 0, QApplication::UnicodeUTF8));
        sklad->setText(QApplication::translate("MainWidget", "\320\241\320\272\320\273\320\260\320\264", 0, QApplication::UnicodeUTF8));
        logout->setText(QApplication::translate("MainWidget", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWidget: public Ui_MainWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIDGET_H
