/********************************************************************************
** Form generated from reading UI file 'edit_sost_tovar.ui'
**
** Created: Fri 1. May 17:35:43 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_SOST_TOVAR_H
#define UI_EDIT_SOST_TOVAR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_sost_tovar
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *add;
    QPushButton *delete_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_sost_tovar)
    {
        if (edit_sost_tovar->objectName().isEmpty())
            edit_sost_tovar->setObjectName(QString::fromUtf8("edit_sost_tovar"));
        edit_sost_tovar->setWindowModality(Qt::ApplicationModal);
        edit_sost_tovar->resize(544, 373);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/status.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_sost_tovar->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_sost_tovar);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_sost_tovar);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        add = new QPushButton(groupBox);
        add->setObjectName(QString::fromUtf8("add"));

        horizontalLayout_2->addWidget(add);

        delete_2 = new QPushButton(groupBox);
        delete_2->setObjectName(QString::fromUtf8("delete_2"));

        horizontalLayout_2->addWidget(delete_2);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_sost_tovar);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_sost_tovar);

        QMetaObject::connectSlotsByName(edit_sost_tovar);
    } // setupUi

    void retranslateUi(QWidget *edit_sost_tovar)
    {
        edit_sost_tovar->setWindowTitle(QApplication::translate("edit_sost_tovar", "\320\222\320\270\320\264\321\213 \321\201\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\271 \321\202\320\276\320\262\320\260\321\200\320\260", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_sost_tovar", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\267\320\260\320\277\320\270\321\201\321\214", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_sost_tovar", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_sost_tovar", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_2->setText(QApplication::translate("edit_sost_tovar", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_sost_tovar: public Ui_edit_sost_tovar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_SOST_TOVAR_H
