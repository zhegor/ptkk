/********************************************************************************
** Form generated from reading UI file 'edit_sklad.ui'
**
** Created: Thu 7. May 15:42:53 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_SKLAD_H
#define UI_EDIT_SKLAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_sklad
{
public:
    QVBoxLayout *verticalLayout_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_sklad)
    {
        if (edit_sklad->objectName().isEmpty())
            edit_sklad->setObjectName(QString::fromUtf8("edit_sklad"));
        edit_sklad->setWindowModality(Qt::ApplicationModal);
        edit_sklad->resize(489, 398);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/warehouse-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_sklad->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_sklad);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tableView = new QTableView(edit_sklad);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_sklad);

        QMetaObject::connectSlotsByName(edit_sklad);
    } // setupUi

    void retranslateUi(QWidget *edit_sklad)
    {
        edit_sklad->setWindowTitle(QApplication::translate("edit_sklad", "\320\241\320\272\320\273\320\260\320\264", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_sklad: public Ui_edit_sklad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_SKLAD_H
