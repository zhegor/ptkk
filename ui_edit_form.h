/********************************************************************************
** Form generated from reading UI file 'edit_form.ui'
**
** Created: Tue 16. Jun 16:17:45 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_FORM_H
#define UI_EDIT_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_form
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *line_name;
    QHBoxLayout *horizontalLayout;
    QPushButton *apply;
    QPushButton *cancel;

    void setupUi(QWidget *edit_form)
    {
        if (edit_form->objectName().isEmpty())
            edit_form->setObjectName(QString::fromUtf8("edit_form"));
        edit_form->resize(297, 91);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_form->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_form);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(edit_form);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        line_name = new QLineEdit(edit_form);
        line_name->setObjectName(QString::fromUtf8("line_name"));

        verticalLayout->addWidget(line_name);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        apply = new QPushButton(edit_form);
        apply->setObjectName(QString::fromUtf8("apply"));
        apply->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(apply);

        cancel = new QPushButton(edit_form);
        cancel->setObjectName(QString::fromUtf8("cancel"));
        cancel->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(cancel);


        verticalLayout_2->addLayout(horizontalLayout);


        retranslateUi(edit_form);

        QMetaObject::connectSlotsByName(edit_form);
    } // setupUi

    void retranslateUi(QWidget *edit_form)
    {
        edit_form->setWindowTitle(QApplication::translate("edit_form", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_form", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        apply->setText(QApplication::translate("edit_form", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        cancel->setText(QApplication::translate("edit_form", "\320\236\321\202\320\274\320\265\320\275\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_form: public Ui_edit_form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_FORM_H
