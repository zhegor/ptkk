#ifndef EDIT_PLATEZH_H
#define EDIT_PLATEZH_H

#include <QWidget>
#include <QSqlRelationalTableModel>

namespace Ui {
    class edit_platezh;
}

class edit_platezh : public QWidget
{
    Q_OBJECT

public:
    explicit edit_platezh(QWidget *parent = 0);
    ~edit_platezh();

private:
    Ui::edit_platezh *ui;

    QSqlRelationalTableModel *view_platezh;

    void print_doc();

private slots:
    void on_show_doc_clicked();
    void on_print_doc_clicked();
    void on_toolButton_clicked();
};

#endif // EDIT_PLATEZH_H
