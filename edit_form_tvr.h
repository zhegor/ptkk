#ifndef EDIT_FORM_TVR_H
#define EDIT_FORM_TVR_H

#include <QWidget>
#include <QDataWidgetMapper>

namespace Ui {
    class edit_form_tvr;
}

class edit_form_tvr : public QWidget
{
    Q_OBJECT

public:
    explicit edit_form_tvr(QWidget *parent = 0);
    ~edit_form_tvr();

    QDataWidgetMapper *mapper;

    void setModel (QAbstractItemModel *model);

private:
    Ui::edit_form_tvr *ui;

private slots:
    void on_cancel_clicked();
    void on_apply_clicked();
};

#endif // EDIT_FORM_TVR_H
