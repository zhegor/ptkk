/********************************************************************************
** Form generated from reading UI file 'edit_form_tvr.ui'
**
** Created: Tue 16. Jun 16:22:20 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_FORM_TVR_H
#define UI_EDIT_FORM_TVR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_form_tvr
{
public:
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *name;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QLineEdit *god;
    QHBoxLayout *horizontalLayout;
    QPushButton *apply;
    QSpacerItem *horizontalSpacer;
    QPushButton *cancel;

    void setupUi(QWidget *edit_form_tvr)
    {
        if (edit_form_tvr->objectName().isEmpty())
            edit_form_tvr->setObjectName(QString::fromUtf8("edit_form_tvr"));
        edit_form_tvr->resize(315, 184);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_form_tvr->setWindowIcon(icon);
        verticalLayout_4 = new QVBoxLayout(edit_form_tvr);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(edit_form_tvr);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        name = new QLineEdit(edit_form_tvr);
        name->setObjectName(QString::fromUtf8("name"));

        verticalLayout->addWidget(name);


        verticalLayout_4->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(edit_form_tvr);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        god = new QLineEdit(edit_form_tvr);
        god->setObjectName(QString::fromUtf8("god"));

        verticalLayout_2->addWidget(god);


        verticalLayout_4->addLayout(verticalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        apply = new QPushButton(edit_form_tvr);
        apply->setObjectName(QString::fromUtf8("apply"));
        apply->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(apply);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        cancel = new QPushButton(edit_form_tvr);
        cancel->setObjectName(QString::fromUtf8("cancel"));
        cancel->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(cancel);


        verticalLayout_4->addLayout(horizontalLayout);


        retranslateUi(edit_form_tvr);

        QMetaObject::connectSlotsByName(edit_form_tvr);
    } // setupUi

    void retranslateUi(QWidget *edit_form_tvr)
    {
        edit_form_tvr->setWindowTitle(QApplication::translate("edit_form_tvr", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_form_tvr", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_form_tvr", "\320\223\320\276\320\264", 0, QApplication::UnicodeUTF8));
        apply->setText(QApplication::translate("edit_form_tvr", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        cancel->setText(QApplication::translate("edit_form_tvr", "\320\236\321\202\320\274\320\265\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_form_tvr: public Ui_edit_form_tvr {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_FORM_TVR_H
