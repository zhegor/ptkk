/********************************************************************************
** Form generated from reading UI file 'mainwindow_mdi.ui'
**
** Created: Tue 16. Jun 17:00:59 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_MDI_H
#define UI_MAINWINDOW_MDI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMdiArea>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mainwindow_mdi
{
public:
    QAction *show_tovar;
    QAction *show_proizv;
    QAction *show_strana;
    QAction *show_categorii;
    QAction *show_sost;
    QAction *set_windows_view;
    QAction *set_tabs_view;
    QAction *show_postavshik;
    QAction *show_sklad;
    QAction *show_postavki;
    QAction *show_prodazha;
    QAction *logout;
    QAction *show_clients;
    QAction *show_banks;
    QAction *show_company_info;
    QAction *show_platezh;
    QAction *show_phaktura;
    QAction *show_help;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QMdiArea *mdiArea;
    QMenuBar *menubar;
    QMenu *menu;
    QMenu *menu_2;
    QMenu *menu_3;
    QMenu *menu_4;
    QMenu *menu_5;
    QMenu *menu_6;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *mainwindow_mdi)
    {
        if (mainwindow_mdi->objectName().isEmpty())
            mainwindow_mdi->setObjectName(QString::fromUtf8("mainwindow_mdi"));
        mainwindow_mdi->resize(692, 438);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        mainwindow_mdi->setWindowIcon(icon);
        show_tovar = new QAction(mainwindow_mdi);
        show_tovar->setObjectName(QString::fromUtf8("show_tovar"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/truck.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_tovar->setIcon(icon1);
        show_proizv = new QAction(mainwindow_mdi);
        show_proizv->setObjectName(QString::fromUtf8("show_proizv"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/proizvod.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_proizv->setIcon(icon2);
        show_strana = new QAction(mainwindow_mdi);
        show_strana->setObjectName(QString::fromUtf8("show_strana"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/strana.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_strana->setIcon(icon3);
        show_categorii = new QAction(mainwindow_mdi);
        show_categorii->setObjectName(QString::fromUtf8("show_categorii"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/category.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_categorii->setIcon(icon4);
        show_sost = new QAction(mainwindow_mdi);
        show_sost->setObjectName(QString::fromUtf8("show_sost"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/status.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_sost->setIcon(icon5);
        set_windows_view = new QAction(mainwindow_mdi);
        set_windows_view->setObjectName(QString::fromUtf8("set_windows_view"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/Browser-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        set_windows_view->setIcon(icon6);
        set_tabs_view = new QAction(mainwindow_mdi);
        set_tabs_view->setObjectName(QString::fromUtf8("set_tabs_view"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/tab-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        set_tabs_view->setIcon(icon7);
        show_postavshik = new QAction(mainwindow_mdi);
        show_postavshik->setObjectName(QString::fromUtf8("show_postavshik"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/post.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_postavshik->setIcon(icon8);
        show_sklad = new QAction(mainwindow_mdi);
        show_sklad->setObjectName(QString::fromUtf8("show_sklad"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/warehouse-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_sklad->setIcon(icon9);
        show_postavki = new QAction(mainwindow_mdi);
        show_postavki->setObjectName(QString::fromUtf8("show_postavki"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/postavka.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_postavki->setIcon(icon10);
        show_prodazha = new QAction(mainwindow_mdi);
        show_prodazha->setObjectName(QString::fromUtf8("show_prodazha"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/prodazha.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_prodazha->setIcon(icon11);
        logout = new QAction(mainwindow_mdi);
        logout->setObjectName(QString::fromUtf8("logout"));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/logout-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        logout->setIcon(icon12);
        show_clients = new QAction(mainwindow_mdi);
        show_clients->setObjectName(QString::fromUtf8("show_clients"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/client.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_clients->setIcon(icon13);
        show_banks = new QAction(mainwindow_mdi);
        show_banks->setObjectName(QString::fromUtf8("show_banks"));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/bank.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_banks->setIcon(icon14);
        show_company_info = new QAction(mainwindow_mdi);
        show_company_info->setObjectName(QString::fromUtf8("show_company_info"));
        show_company_info->setIcon(icon2);
        show_platezh = new QAction(mainwindow_mdi);
        show_platezh->setObjectName(QString::fromUtf8("show_platezh"));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/doc.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_platezh->setIcon(icon15);
        show_phaktura = new QAction(mainwindow_mdi);
        show_phaktura->setObjectName(QString::fromUtf8("show_phaktura"));
        show_phaktura->setIcon(icon15);
        show_help = new QAction(mainwindow_mdi);
        show_help->setObjectName(QString::fromUtf8("show_help"));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/Help.png"), QSize(), QIcon::Normal, QIcon::Off);
        show_help->setIcon(icon16);
        centralwidget = new QWidget(mainwindow_mdi);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        mdiArea = new QMdiArea(centralwidget);
        mdiArea->setObjectName(QString::fromUtf8("mdiArea"));
        mdiArea->setAutoFillBackground(false);
        mdiArea->setStyleSheet(QString::fromUtf8("#mdiArea{ background-image: url(:/images/prk_logo_transparent.png);\n"
"background-position: center center;\n"
"background-repeat: no-repeat;\n"
"}"));
        mdiArea->setFrameShadow(QFrame::Raised);
        QBrush brush(QColor(160, 160, 160, 255));
        brush.setStyle(Qt::NoBrush);
        mdiArea->setBackground(brush);

        verticalLayout->addWidget(mdiArea);

        mainwindow_mdi->setCentralWidget(centralwidget);
        menubar = new QMenuBar(mainwindow_mdi);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 692, 21));
        menu = new QMenu(menubar);
        menu->setObjectName(QString::fromUtf8("menu"));
        menu_2 = new QMenu(menubar);
        menu_2->setObjectName(QString::fromUtf8("menu_2"));
        menu_3 = new QMenu(menubar);
        menu_3->setObjectName(QString::fromUtf8("menu_3"));
        menu_4 = new QMenu(menubar);
        menu_4->setObjectName(QString::fromUtf8("menu_4"));
        menu_5 = new QMenu(menubar);
        menu_5->setObjectName(QString::fromUtf8("menu_5"));
        menu_6 = new QMenu(menubar);
        menu_6->setObjectName(QString::fromUtf8("menu_6"));
        mainwindow_mdi->setMenuBar(menubar);
        statusbar = new QStatusBar(mainwindow_mdi);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        mainwindow_mdi->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menubar->addAction(menu_4->menuAction());
        menubar->addAction(menu_2->menuAction());
        menubar->addAction(menu_5->menuAction());
        menubar->addAction(menu_3->menuAction());
        menubar->addAction(menu_6->menuAction());
        menu->addAction(show_tovar);
        menu->addSeparator();
        menu->addAction(show_categorii);
        menu->addAction(show_sost);
        menu->addSeparator();
        menu_2->addAction(show_proizv);
        menu_2->addAction(show_strana);
        menu_2->addSeparator();
        menu_2->addAction(show_postavshik);
        menu_2->addAction(show_clients);
        menu_2->addSeparator();
        menu_2->addAction(show_banks);
        menu_2->addSeparator();
        menu_2->addAction(show_company_info);
        menu_3->addAction(logout);
        menu_3->addSeparator();
        menu_3->addAction(set_windows_view);
        menu_3->addAction(set_tabs_view);
        menu_4->addAction(show_postavki);
        menu_4->addSeparator();
        menu_4->addAction(show_prodazha);
        menu_4->addSeparator();
        menu_4->addAction(show_sklad);
        menu_5->addAction(show_platezh);
        menu_5->addSeparator();
        menu_5->addAction(show_phaktura);
        menu_6->addAction(show_help);

        retranslateUi(mainwindow_mdi);

        QMetaObject::connectSlotsByName(mainwindow_mdi);
    } // setupUi

    void retranslateUi(QMainWindow *mainwindow_mdi)
    {
        mainwindow_mdi->setWindowTitle(QApplication::translate("mainwindow_mdi", "\320\236\320\236\320\236 \"\320\237\321\200\320\270\320\274\320\276\321\200\321\201\320\272\320\260\321\217 \320\242\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\275\320\260\321\217 \320\232\320\276\320\274\320\277\320\260\320\275\320\270\321\217\"", 0, QApplication::UnicodeUTF8));
        show_tovar->setText(QApplication::translate("mainwindow_mdi", "\320\237\320\265\321\200\320\265\321\207\320\265\320\275\321\214 \321\202\320\276\320\262\320\260\321\200\320\276\320\262", 0, QApplication::UnicodeUTF8));
        show_proizv->setText(QApplication::translate("mainwindow_mdi", "\320\232\320\276\320\274\320\277\320\260\320\275\320\270\320\270-\320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\320\270", 0, QApplication::UnicodeUTF8));
        show_strana->setText(QApplication::translate("mainwindow_mdi", "\320\241\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272 \321\201\321\202\321\200\320\260\320\275-\320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\320\265\320\271", 0, QApplication::UnicodeUTF8));
        show_categorii->setText(QApplication::translate("mainwindow_mdi", "\320\237\320\265\321\200\320\265\321\207\320\265\320\275\321\214 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\271", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        show_categorii->setToolTip(QApplication::translate("mainwindow_mdi", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\277\320\265\321\200\320\265\321\207\320\265\320\275\321\214 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\271", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        show_sost->setText(QApplication::translate("mainwindow_mdi", "\320\222\320\270\320\264\321\213 \321\201\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\271 \321\202\320\276\320\262\320\260\321\200\320\276\320\262", 0, QApplication::UnicodeUTF8));
        set_windows_view->setText(QApplication::translate("mainwindow_mdi", "\320\236\321\202\320\276\320\261\321\200\320\260\320\266\320\260\321\202\321\214 \320\262 \320\262\320\270\320\264\320\265 \320\276\320\272\320\276\320\275", 0, QApplication::UnicodeUTF8));
        set_tabs_view->setText(QApplication::translate("mainwindow_mdi", "\320\236\321\202\320\276\320\261\321\200\320\260\320\266\320\260\321\202\321\214 \320\262 \320\262\320\270\320\264\320\265 \320\262\320\272\320\273\320\260\320\264\320\276\320\272", 0, QApplication::UnicodeUTF8));
        show_postavshik->setText(QApplication::translate("mainwindow_mdi", "\320\237\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272\320\270", 0, QApplication::UnicodeUTF8));
        show_sklad->setText(QApplication::translate("mainwindow_mdi", "\320\241\320\272\320\273\320\260\320\264", 0, QApplication::UnicodeUTF8));
        show_postavki->setText(QApplication::translate("mainwindow_mdi", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        show_prodazha->setText(QApplication::translate("mainwindow_mdi", "\320\237\321\200\320\276\320\264\320\260\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        logout->setText(QApplication::translate("mainwindow_mdi", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
        show_clients->setText(QApplication::translate("mainwindow_mdi", "\320\232\320\273\320\270\320\265\320\275\321\202\321\213", 0, QApplication::UnicodeUTF8));
        show_banks->setText(QApplication::translate("mainwindow_mdi", "\320\221\320\260\320\275\320\272\320\270", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        show_banks->setToolTip(QApplication::translate("mainwindow_mdi", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \321\201\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272 \320\261\320\260\320\275\320\272\320\276\320\262", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        show_banks->setWhatsThis(QApplication::translate("mainwindow_mdi", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \321\201\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272 \320\261\320\260\320\275\320\272\320\276\320\262", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        show_company_info->setText(QApplication::translate("mainwindow_mdi", "\320\230\320\275\321\204\320\276\321\200\320\274\320\260\321\206\320\270\321\217 \320\276 \320\277\321\200\320\265\320\264\320\277\321\200\320\270\321\217\321\202\320\270\320\270", 0, QApplication::UnicodeUTF8));
        show_platezh->setText(QApplication::translate("mainwindow_mdi", "\320\237\320\273\320\260\321\202\320\265\320\266\320\275\321\213\320\265 \320\277\320\276\321\200\321\203\321\207\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        show_phaktura->setText(QApplication::translate("mainwindow_mdi", "\320\241\321\207\320\265\321\202\320\260-\321\204\320\260\320\272\321\202\321\203\321\200\321\213", 0, QApplication::UnicodeUTF8));
        show_help->setText(QApplication::translate("mainwindow_mdi", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", 0, QApplication::UnicodeUTF8));
        show_help->setShortcut(QApplication::translate("mainwindow_mdi", "F1", 0, QApplication::UnicodeUTF8));
        menu->setTitle(QApplication::translate("mainwindow_mdi", "\320\242\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        menu_2->setTitle(QApplication::translate("mainwindow_mdi", "\320\232\320\276\320\274\320\277\320\260\320\275\320\270\320\270", 0, QApplication::UnicodeUTF8));
        menu_3->setTitle(QApplication::translate("mainwindow_mdi", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0, QApplication::UnicodeUTF8));
        menu_4->setTitle(QApplication::translate("mainwindow_mdi", "\320\236\320\277\320\265\321\200\320\260\321\206\320\270\320\270 \321\201 \321\202\320\276\320\262\320\260\321\200\320\276\320\274", 0, QApplication::UnicodeUTF8));
        menu_5->setTitle(QApplication::translate("mainwindow_mdi", "\320\244\320\270\320\275\320\260\320\275\321\201\320\276\320\262\321\213\320\265 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202\321\213", 0, QApplication::UnicodeUTF8));
        menu_6->setTitle(QApplication::translate("mainwindow_mdi", "\320\236 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class mainwindow_mdi: public Ui_mainwindow_mdi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_MDI_H
