/********************************************************************************
** Form generated from reading UI file 'edit_onecolumn.ui'
**
** Created: Tue 16. Jun 16:26:50 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_ONECOLUMN_H
#define UI_EDIT_ONECOLUMN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_onecolumn
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLineEdit *name_line;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *add_smth;
    QPushButton *delete_smth;
    QTableView *tableView;

    void setupUi(QWidget *edit_onecolumn)
    {
        if (edit_onecolumn->objectName().isEmpty())
            edit_onecolumn->setObjectName(QString::fromUtf8("edit_onecolumn"));
        edit_onecolumn->resize(408, 334);
        verticalLayout_2 = new QVBoxLayout(edit_onecolumn);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_onecolumn);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"font: 10pt \"MS Shell Dlg 2\";\n"
"}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        name_line = new QLineEdit(groupBox);
        name_line->setObjectName(QString::fromUtf8("name_line"));

        horizontalLayout->addWidget(name_line);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        add_smth = new QPushButton(groupBox);
        add_smth->setObjectName(QString::fromUtf8("add_smth"));
        add_smth->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_2->addWidget(add_smth);

        delete_smth = new QPushButton(groupBox);
        delete_smth->setObjectName(QString::fromUtf8("delete_smth"));
        delete_smth->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_2->addWidget(delete_smth);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_onecolumn);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_onecolumn);

        QMetaObject::connectSlotsByName(edit_onecolumn);
    } // setupUi

    void retranslateUi(QWidget *edit_onecolumn)
    {
        edit_onecolumn->setWindowTitle(QApplication::translate("edit_onecolumn", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("edit_onecolumn", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\270", 0, QApplication::UnicodeUTF8));
        add_smth->setText(QApplication::translate("edit_onecolumn", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_smth->setText(QApplication::translate("edit_onecolumn", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_onecolumn: public Ui_edit_onecolumn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_ONECOLUMN_H
