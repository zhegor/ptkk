/********************************************************************************
** Form generated from reading UI file 'edit_proizv.ui'
**
** Created: Tue 16. Jun 16:48:34 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_PROIZV_H
#define UI_EDIT_PROIZV_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_proizv
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *proizv_name;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QComboBox *combo_strana;
    QHBoxLayout *horizontalLayout;
    QPushButton *add;
    QPushButton *delete_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_proizv)
    {
        if (edit_proizv->objectName().isEmpty())
            edit_proizv->setObjectName(QString::fromUtf8("edit_proizv"));
        edit_proizv->resize(503, 434);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/proizvod.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_proizv->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_proizv);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_proizv);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"font: 10pt \"MS Shell Dlg 2\";}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        proizv_name = new QLineEdit(groupBox);
        proizv_name->setObjectName(QString::fromUtf8("proizv_name"));

        horizontalLayout_2->addWidget(proizv_name);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_3->addWidget(label_2);

        combo_strana = new QComboBox(groupBox);
        combo_strana->setObjectName(QString::fromUtf8("combo_strana"));

        horizontalLayout_3->addWidget(combo_strana);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        add = new QPushButton(groupBox);
        add->setObjectName(QString::fromUtf8("add"));
        add->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";\n"
""));

        horizontalLayout->addWidget(add);

        delete_2 = new QPushButton(groupBox);
        delete_2->setObjectName(QString::fromUtf8("delete_2"));
        delete_2->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(delete_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_proizv);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_proizv);

        QMetaObject::connectSlotsByName(edit_proizv);
    } // setupUi

    void retranslateUi(QWidget *edit_proizv)
    {
        edit_proizv->setWindowTitle(QApplication::translate("edit_proizv", "\320\237\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\320\270", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_proizv", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_proizv", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265 \320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_proizv", "\320\241\321\202\321\200\320\260\320\275\320\260", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_proizv", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_2->setText(QApplication::translate("edit_proizv", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_proizv: public Ui_edit_proizv {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_PROIZV_H
