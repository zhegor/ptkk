#include "edit_sklad.h"
#include "ui_edit_sklad.h"

#include <QMessageBox>
#include <QSqlQuery>
#include <QDesktopWidget>

edit_sklad::edit_sklad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_sklad)
{
    ui->setupUi(this);

    //relation table model
    mdl = new QSqlRelationalTableModel();
    mdl->setTable("sklad");
    mdl->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    mdl->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    mdl->setRelation(5,QSqlRelation("postavshiki","post_id","post_name"));
    mdl->select();
    ui->tableView->setModel(mdl);

    mdl->setHeaderData(1,Qt::Horizontal, "���������");
    mdl->setHeaderData(2,Qt::Horizontal, "�����");
    mdl->setHeaderData(3,Qt::Horizontal, "������");
    mdl->setHeaderData(4,Qt::Horizontal, "��� ������������");
    mdl->setHeaderData(5,Qt::Horizontal, "���������");
    mdl->setHeaderData(6,Qt::Horizontal, "���������� (��.)");
    mdl->setHeaderData(7,Qt::Horizontal, "���� ���������� (���.)");
    mdl->setHeaderData(8,Qt::Horizontal, "���� � ������ ������� (���.)");

    mdl->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    mdl->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    mdl->setRelation(3,QSqlRelation("strana_pr","strana_id","strana_name"));
    mdl->setRelation(5,QSqlRelation("postavshiki","post_id","post_name"));

    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);

    ui->tableView->resizeColumnsToContents();

    //������� ���� � ����� ������
    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - (this->width()/2));
    center.setY(center.y() - (this->height()/2));
    move(center);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_sklad::~edit_sklad()
{
    delete ui;
}

