#include "mainwindow_mdi.h"
#include "ui_mainwindow_mdi.h"

#include <QDesktopWidget>
#include <QDesktopServices>
#include <QUrl>

mainwindow_mdi::mainwindow_mdi(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mainwindow_mdi)
{
    ui->setupUi(this);

    ui->mdiArea->setViewMode(QMdiArea::TabbedView);

    //������� ���� � ����� ������
    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - (this->width()/2));
    center.setY(center.y() - (this->height()/2));
    move(center);
}

mainwindow_mdi::~mainwindow_mdi()
{
    delete ui;
}

//����� ������� ����������
int mainwindow_mdi::user_check(int user_level)
{
    //������������ 1
    if(user_level == 1)
      {
        ui->menubar->actions()[1]->setVisible(false);
        ui->menubar->actions()[3]->setVisible(false);

        return 0;
      }
    else{

        //������������ 2
        if (user_level == 2)
        {
            ui->menubar->actions()[0]->setVisible(false);
            ui->menubar->actions()[2]->setVisible(false);

            return 0;
        }
        }

}

//�����
void mainwindow_mdi::on_show_banks_triggered()
{
    show_banks = new edit_banks();
    ui->mdiArea->addSubWindow(show_banks);
    show_banks->show();
}

//��������� ������
void mainwindow_mdi::on_show_categorii_triggered()
{
   show_onecolumn = new edit_onecolumn();
   ui->mdiArea->addSubWindow(show_onecolumn);
   show_onecolumn->change_to_categorii();
   show_onecolumn->show();
}

//�������
void mainwindow_mdi::on_show_clients_triggered()
{
   show_clients = new edit_clients();
   ui->mdiArea->addSubWindow(show_clients);
   show_clients->change_to_clients();
   show_clients->show();
}

//��������� ���������
void mainwindow_mdi::on_show_platezh_triggered()
{
    show_platezh = new edit_platezh();
    ui->mdiArea->addSubWindow(show_platezh);
    show_platezh->show();
}

//����������
void mainwindow_mdi::on_show_postavshik_triggered()
{
     show_clients = new edit_clients();
     ui->mdiArea->addSubWindow(show_clients);
     show_clients->change_to_postavshiki();
     show_clients->show();
}

//�������
void mainwindow_mdi::on_show_prodazha_triggered()
{
    show_prodazha = new edit_prod();
    ui->mdiArea->addSubWindow(show_prodazha);
    show_prodazha->show();
}

//��������
void mainwindow_mdi::on_show_postavki_triggered()
{
    show_postavki = new edit_postavki();
    ui->mdiArea->addSubWindow(show_postavki);
    show_postavki->show();
}

//�������������
void mainwindow_mdi::on_show_proizv_triggered()
{
    show_proizv = new edit_proizv();
    ui->mdiArea->addSubWindow(show_proizv);
    show_proizv->show();
}

//�������
void mainwindow_mdi::on_show_phaktura_triggered()
{
    show_phaktura = new edit_phaktura();
    ui->mdiArea->addSubWindow(show_phaktura);
    show_phaktura->show();
}

//�����
void mainwindow_mdi::on_show_sklad_triggered()
{
    show_sklad = new edit_sklad();
    ui->mdiArea->addSubWindow(show_sklad);
    show_sklad->show();
}

//���������
void mainwindow_mdi::on_show_sost_triggered()
{
    show_onecolumn = new edit_onecolumn();
    ui->mdiArea->addSubWindow(show_onecolumn);
    show_onecolumn->change_to_sost();
    show_onecolumn->show();
}

//������
void mainwindow_mdi::on_show_strana_triggered()
{
    show_onecolumn = new edit_onecolumn();
    ui->mdiArea->addSubWindow(show_onecolumn);
    show_onecolumn->change_to_strana();
    show_onecolumn->show();
}

void mainwindow_mdi::on_show_tovar_triggered()
{
    show_tovar = new edit_tovar();
    ui->mdiArea->addSubWindow(show_tovar);
    show_tovar->show();
}

//� ���� ����
void mainwindow_mdi::on_set_windows_view_triggered()
{
    ui->mdiArea->setViewMode(QMdiArea::SubWindowView);
}

//�������
void mainwindow_mdi::on_set_tabs_view_triggered()
{
    ui->mdiArea->setViewMode(QMdiArea::TabbedView);
}

//������� �����
void mainwindow_mdi::on_logout_triggered()
{
    close();
    loginDialog *login_win = new loginDialog();
    login_win->show();
}

void mainwindow_mdi::on_show_company_info_triggered()
{
    show_ptk = new edit_ptk_info();
    ui->mdiArea->addSubWindow(show_ptk);
    show_ptk->show();
}

void mainwindow_mdi::on_show_help_triggered()
{
    QDesktopServices::openUrl(QUrl(QCoreApplication::applicationDirPath() +
                                   "/docs/help.chm"));
}
