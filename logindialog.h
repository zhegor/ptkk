#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>

#include "mainwindow_mdi.h"

/**
  * @namespace ������������ ������������ Ui
  * @class ������������ ������� loginDialog
*/
namespace Ui {
    class loginDialog;
}

/**
  * @class loginDialog
  * ��������� ����� �����������. ����������� �� QDialog.
*/

class loginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit loginDialog(QWidget *parent = 0);
    ~loginDialog();

private:
    Ui::loginDialog *ui;

private slots:
    /**
      *���� �������������� ������ ������
      */
    void on_buttonBox_rejected();
    /**
      *���� �������������� ������ ������������� �����������
      */
    void on_buttonBox_accepted();
};

#endif // LOGINDIALOG_H
