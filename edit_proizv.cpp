#include "edit_proizv.h"
#include "ui_edit_proizv.h"

#include <QMessageBox>

edit_proizv::edit_proizv(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_proizv)
{
    ui->setupUi(this);

    //������� ��������������
    view_proizv = new QSqlRelationalTableModel(this);
    view_proizv->setTable("proizvod");
    view_proizv->setRelation(2,QSqlRelation("strana_pr","strana_id","strana_name"));
    view_proizv->setHeaderData(1,Qt::Horizontal, "������������");
    view_proizv->setHeaderData(2,Qt::Horizontal, "������");
    view_proizv->select();



    ui->tableView->setModel(view_proizv);
    ui->tableView->hideColumn(0);

    //������ �����
    QSqlQueryModel *strani = new QSqlQueryModel();
    strani->setQuery("SELECT strana_name, strana_id FROM strana_pr");
    ui->combo_strana->setModel(strani);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);


}

edit_proizv::~edit_proizv()
{
    delete ui;
}


void edit_proizv::on_add_clicked()
{
    //���� �� ��������� ����
    if(ui->proizv_name->text().isEmpty())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������
    else
    {
      int row = 0;
      view_proizv->insertRows(row, 1);
      QModelIndex proizv_name = view_proizv->index(row, 1);
      view_proizv->setData(proizv_name, ui->proizv_name->text());

      //����������� id ������ �� ������
      QModelIndex index = ui->combo_strana->model()->index(ui->combo_strana->currentIndex(),1);
      QString strana_id = ui->combo_strana->model()->data(index).toString();

      QModelIndex proizv_strana = view_proizv->index(row, 2);
      view_proizv->setData(proizv_strana, strana_id);

      view_proizv->submitAll();
      ui->proizv_name->setText("");
      ui->tableView->scrollToBottom();

       }
}

void edit_proizv::on_delete_2_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ������?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    view_proizv->removeRow(index.row());
    }
}
