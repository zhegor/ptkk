/********************************************************************************
** Form generated from reading UI file 'edit_postavki.ui'
**
** Created: Tue 16. Jun 16:48:33 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_POSTAVKI_H
#define UI_EDIT_POSTAVKI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_postavki
{
public:
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *comboBox_post;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBox_categorii;
    QToolButton *showAll;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QTableView *tovarTable;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *kolvo_line;
    QLineEdit *tsena_line;
    QLineEdit *natsen_line;
    QSpacerItem *horizontalSpacer;
    QPushButton *add;
    QLabel *label_4;
    QTableView *skladView;

    void setupUi(QWidget *edit_postavki)
    {
        if (edit_postavki->objectName().isEmpty())
            edit_postavki->setObjectName(QString::fromUtf8("edit_postavki"));
        edit_postavki->setWindowModality(Qt::ApplicationModal);
        edit_postavki->resize(705, 677);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/postavka.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_postavki->setWindowIcon(icon);
        verticalLayout_6 = new QVBoxLayout(edit_postavki);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(edit_postavki);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"	font: 10pt \"MS Shell Dlg 2\";\n"
"}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        comboBox_post = new QComboBox(groupBox);
        comboBox_post->setObjectName(QString::fromUtf8("comboBox_post"));

        horizontalLayout_2->addWidget(comboBox_post);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        comboBox_categorii = new QComboBox(groupBox);
        comboBox_categorii->setObjectName(QString::fromUtf8("comboBox_categorii"));

        horizontalLayout->addWidget(comboBox_categorii);


        verticalLayout->addLayout(horizontalLayout);

        showAll = new QToolButton(groupBox);
        showAll->setObjectName(QString::fromUtf8("showAll"));
        showAll->setStyleSheet(QString::fromUtf8("font: 9pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(showAll);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));

        horizontalLayout_4->addLayout(verticalLayout_2);

        tovarTable = new QTableView(groupBox);
        tovarTable->setObjectName(QString::fromUtf8("tovarTable"));
        tovarTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tovarTable->horizontalHeader()->setStretchLastSection(true);

        horizontalLayout_4->addWidget(tovarTable);


        verticalLayout_3->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_4->addWidget(label_3);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_4->addWidget(label_5);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_4->addWidget(label_6);


        horizontalLayout_3->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        kolvo_line = new QLineEdit(groupBox);
        kolvo_line->setObjectName(QString::fromUtf8("kolvo_line"));
        kolvo_line->setMaximumSize(QSize(40, 16777215));

        verticalLayout_5->addWidget(kolvo_line);

        tsena_line = new QLineEdit(groupBox);
        tsena_line->setObjectName(QString::fromUtf8("tsena_line"));
        tsena_line->setMaximumSize(QSize(90, 16777215));

        verticalLayout_5->addWidget(tsena_line);

        natsen_line = new QLineEdit(groupBox);
        natsen_line->setObjectName(QString::fromUtf8("natsen_line"));
        natsen_line->setMaximumSize(QSize(40, 16777215));

        verticalLayout_5->addWidget(natsen_line);


        horizontalLayout_3->addLayout(verticalLayout_5);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_3);


        verticalLayout_6->addWidget(groupBox);

        add = new QPushButton(edit_postavki);
        add->setObjectName(QString::fromUtf8("add"));
        add->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_6->addWidget(add);

        label_4 = new QLabel(edit_postavki);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setStyleSheet(QString::fromUtf8("font: 75 8pt \"MS Shell Dlg 2\";\n"
"font: 10pt \"MS Shell Dlg 2\";\n"
"font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_6->addWidget(label_4);

        skladView = new QTableView(edit_postavki);
        skladView->setObjectName(QString::fromUtf8("skladView"));

        verticalLayout_6->addWidget(skladView);


        retranslateUi(edit_postavki);

        QMetaObject::connectSlotsByName(edit_postavki);
    } // setupUi

    void retranslateUi(QWidget *edit_postavki)
    {
        edit_postavki->setWindowTitle(QApplication::translate("edit_postavki", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_postavki", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_postavki", "\320\237\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_postavki", "\320\232\320\260\321\202\320\265\320\263\320\276\321\200\320\270\321\217 \321\202\320\276\320\262\320\260\321\200\320\260", 0, QApplication::UnicodeUTF8));
        showAll->setText(QApplication::translate("edit_postavki", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\262\321\201\321\221", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("edit_postavki", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 (\321\210\321\202.)", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("edit_postavki", "\320\246\320\265\320\275\320\260 (\321\200\321\203\320\261.)", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("edit_postavki", "\320\235\320\260\321\206\320\265\320\275\320\272\320\260 (%)", 0, QApplication::UnicodeUTF8));
        natsen_line->setText(QApplication::translate("edit_postavki", "18", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_postavki", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \321\202\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_postavki", "\320\241\320\272\320\273\320\260\320\264", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_postavki: public Ui_edit_postavki {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_POSTAVKI_H
