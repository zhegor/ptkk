#include "edit_clients.h"
#include "ui_edit_clients.h"

#include <QMessageBox>
#include <QSqlQuery>

edit_clients::edit_clients(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_clients)
{
    ui->setupUi(this);

    QSqlQueryModel *banks = new QSqlQueryModel();
    banks->setQuery("SELECT bank_name,bank_id FROM banks");
    ui->bank_combo->setModel(banks);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_clients::~edit_clients()
{
    delete ui;
}


//������� ������� �� �����������
void edit_clients::change_to_postavshiki()
{
    this->setWindowTitle("����������");

    view_clients = new QSqlRelationalTableModel();
    view_clients->setTable("postavshiki");
    view_clients->setRelation(7,QSqlRelation("banks","bank_id","bank_name"));
    view_clients->select();
    ui->tableView->setModel(view_clients);

    ui->tableView->hideColumn(0);
    ui->tableView->resizeColumnsToContents();
    view_clients->setHeaderData(1, Qt::Horizontal, "���������");
    view_clients->setHeaderData(2, Qt::Horizontal, "�����");
    view_clients->setHeaderData(3, Qt::Horizontal, "E-Mail");
    view_clients->setHeaderData(4, Qt::Horizontal, "�������");
    view_clients->setHeaderData(5, Qt::Horizontal, "���");
    view_clients->setHeaderData(6, Qt::Horizontal, "���");
    view_clients->setHeaderData(7, Qt::Horizontal, "����");
    view_clients->setHeaderData(8, Qt::Horizontal, "����");
}

//...�� ��������
void edit_clients::change_to_clients()
{
    this->setWindowTitle("�������");

    view_clients = new QSqlRelationalTableModel();
    view_clients->setTable("clients");
    view_clients->setRelation(7,QSqlRelation("banks","bank_id","bank_name"));
    view_clients->select();
    ui->tableView->setModel(view_clients);

    ui->tableView->hideColumn(0);
    ui->tableView->resizeColumnsToContents();
    view_clients->setHeaderData(1, Qt::Horizontal, "������");
    view_clients->setHeaderData(2, Qt::Horizontal, "�����");
    view_clients->setHeaderData(3, Qt::Horizontal, "E-Mail");
    view_clients->setHeaderData(4, Qt::Horizontal, "�������");
    view_clients->setHeaderData(5, Qt::Horizontal, "���");
    view_clients->setHeaderData(6, Qt::Horizontal, "���");
    view_clients->setHeaderData(7, Qt::Horizontal, "����");
    view_clients->setHeaderData(8, Qt::Horizontal, "����");
}


//�������� �������
void edit_clients::on_add_client_clicked()
{
    //���� ���� �� ���������, ����� ������
    if(ui->client_adress_line->text().isEmpty() ||
       ui->client_email_line->text().isEmpty() ||
       ui->client_inn_line->text().isEmpty() ||
       ui->client_kpp_line->text().isEmpty() ||
       ui->client_name_line->text().isEmpty() ||
       ui->client_phone_line->text().isEmpty() ||
       ui->client_schet_line->text().isEmpty())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������, ��������� ������
    else
    {
      int row = 0;
      view_clients->insertRows(row, 1);

      //���
      QModelIndex client_name_line = view_clients->index(row, 1);
      view_clients->setData(client_name_line, ui->client_name_line->text());
      //�����
      QModelIndex client_adress_line = view_clients->index(row, 2);
      view_clients->setData(client_adress_line, ui->client_adress_line->text());
      //�����
      QModelIndex client_email_line = view_clients->index(row, 3);
      view_clients->setData(client_email_line, ui->client_email_line->text());
      //�������
      QModelIndex client_phone_line = view_clients->index(row, 4);
      view_clients->setData(client_phone_line, ui->client_phone_line->text());
      //���
      QModelIndex client_inn_line = view_clients->index(row, 5);
      view_clients->setData(client_inn_line, ui->client_inn_line->text());
      //���
      QModelIndex client_kpp_line = view_clients->index(row, 6);
      view_clients->setData(client_kpp_line, ui->client_kpp_line->text());
      //�������� id �����
      QModelIndex index = ui->bank_combo->model()->index(ui->bank_combo->currentIndex(),1);
      QString bank_id = ui->bank_combo->model()->data(index).toString();
      //���� �������
      QModelIndex client_bank = view_clients->index(row, 7);
      view_clients->setData(client_bank, bank_id);
      //���� � �����
      QModelIndex client_schet_line = view_clients->index(row, 8);
      view_clients->setData(client_schet_line, ui->client_schet_line->text());


      view_clients->submitAll();
      ui->client_name_line->setText("");
      ui->client_email_line->setText("");
      ui->client_adress_line->setText("");
      ui->client_inn_line->setText("");
      ui->client_kpp_line->setText("");
      ui->client_phone_line->setText("");
      ui->client_schet_line->setText("");
      ui->tableView->scrollToBottom();

}
}

//������� �������
void edit_clients::on_delete_client_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ������?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    view_clients->removeRow(index.row());
    }
}
