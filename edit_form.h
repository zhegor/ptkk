#ifndef EDIT_FORM_H
#define EDIT_FORM_H

#include <QWidget>
#include <QDataWidgetMapper>

namespace Ui {
    class edit_form;
}

class edit_form : public QWidget
{
    Q_OBJECT

public:
    explicit edit_form(QWidget *parent = 0);
    ~edit_form();

    QDataWidgetMapper *mapper;

    void setModel (QAbstractItemModel *model);

private:
    Ui::edit_form *ui;

private slots:

    void on_cancel_clicked();
    void on_apply_clicked();
};

#endif // EDIT_FORM_H
