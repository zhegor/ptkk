/********************************************************************************
** Form generated from reading UI file 'edit_phaktura.ui'
**
** Created: Tue 16. Jun 16:31:11 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_PHAKTURA_H
#define UI_EDIT_PHAKTURA_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTableView>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_phaktura
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTableView *tableView;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QTableView *tableView_2;
    QHBoxLayout *horizontalLayout;
    QToolButton *save_doc;
    QToolButton *print_doc;
    QToolButton *show_doc;

    void setupUi(QWidget *edit_phaktura)
    {
        if (edit_phaktura->objectName().isEmpty())
            edit_phaktura->setObjectName(QString::fromUtf8("edit_phaktura"));
        edit_phaktura->resize(503, 498);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/doc.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_phaktura->setWindowIcon(icon);
        verticalLayout_3 = new QVBoxLayout(edit_phaktura);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(edit_phaktura);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(label);

        tableView = new QTableView(edit_phaktura);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSortingEnabled(true);

        verticalLayout->addWidget(tableView);


        verticalLayout_3->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(edit_phaktura);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        verticalLayout_2->addWidget(label_2);

        tableView_2 = new QTableView(edit_phaktura);
        tableView_2->setObjectName(QString::fromUtf8("tableView_2"));
        tableView_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView_2->setSortingEnabled(true);

        verticalLayout_2->addWidget(tableView_2);


        verticalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        save_doc = new QToolButton(edit_phaktura);
        save_doc->setObjectName(QString::fromUtf8("save_doc"));
        save_doc->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";\n"
""));

        horizontalLayout->addWidget(save_doc);

        print_doc = new QToolButton(edit_phaktura);
        print_doc->setObjectName(QString::fromUtf8("print_doc"));
        print_doc->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(print_doc);

        show_doc = new QToolButton(edit_phaktura);
        show_doc->setObjectName(QString::fromUtf8("show_doc"));
        show_doc->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout->addWidget(show_doc);


        verticalLayout_3->addLayout(horizontalLayout);


        retranslateUi(edit_phaktura);

        QMetaObject::connectSlotsByName(edit_phaktura);
    } // setupUi

    void retranslateUi(QWidget *edit_phaktura)
    {
        edit_phaktura->setWindowTitle(QApplication::translate("edit_phaktura", "\320\241\321\207\320\265\321\202-\321\204\320\260\320\272\321\202\321\203\321\200\320\260", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_phaktura", "\320\241\321\207\320\265\321\202-\321\204\320\260\320\272\321\202\321\203\321\200\320\260", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_phaktura", "\320\242\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        save_doc->setText(QApplication::translate("edit_phaktura", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
        print_doc->setText(QApplication::translate("edit_phaktura", "\320\235\320\260\320\277\320\265\321\207\320\260\321\202\320\260\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
        show_doc->setText(QApplication::translate("edit_phaktura", "\320\237\320\276\320\272\320\260\320\267\320\260\321\202\321\214 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_phaktura: public Ui_edit_phaktura {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_PHAKTURA_H
