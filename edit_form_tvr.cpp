#include "edit_form_tvr.h"
#include "ui_edit_form_tvr.h"

#include <QDesktopWidget>

edit_form_tvr::edit_form_tvr(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_form_tvr)
{
    ui->setupUi(this);

    mapper = new QDataWidgetMapper(this);
    mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    //������� ���� � ����� ������
    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - (this->width()/2));
    center.setY(center.y() - (this->height()/2));
    move(center);
}

edit_form_tvr::~edit_form_tvr()
{
    delete ui;
}

void edit_form_tvr::setModel(QAbstractItemModel *model)
{
    mapper->setModel(model);
    mapper->addMapping(ui->name,2);
    mapper->addMapping(ui->god,6);
}

void edit_form_tvr::on_apply_clicked()
{
    mapper->submit();
    close();
}

void edit_form_tvr::on_cancel_clicked()
{
    close();
}
