/********************************************************************************
** Form generated from reading UI file 'edit_form_skld.ui'
**
** Created: Mon 16. Mar 20:20:42 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_FORM_SKLD_H
#define UI_EDIT_FORM_SKLD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_form_skld
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *apply;
    QPushButton *cancel;

    void setupUi(QWidget *edit_form_skld)
    {
        if (edit_form_skld->objectName().isEmpty())
            edit_form_skld->setObjectName(QString::fromUtf8("edit_form_skld"));
        edit_form_skld->resize(208, 137);
        verticalLayout_3 = new QVBoxLayout(edit_form_skld);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(edit_form_skld);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        spinBox = new QSpinBox(edit_form_skld);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        verticalLayout_2->addWidget(spinBox);


        verticalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        apply = new QPushButton(edit_form_skld);
        apply->setObjectName(QString::fromUtf8("apply"));

        horizontalLayout->addWidget(apply);

        cancel = new QPushButton(edit_form_skld);
        cancel->setObjectName(QString::fromUtf8("cancel"));

        horizontalLayout->addWidget(cancel);


        verticalLayout_3->addLayout(horizontalLayout);


        retranslateUi(edit_form_skld);

        QMetaObject::connectSlotsByName(edit_form_skld);
    } // setupUi

    void retranslateUi(QWidget *edit_form_skld)
    {
        edit_form_skld->setWindowTitle(QApplication::translate("edit_form_skld", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_form_skld", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276", 0, QApplication::UnicodeUTF8));
        apply->setText(QApplication::translate("edit_form_skld", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214 \320\270\320\267\320\274\320\265\320\275\320\265\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        cancel->setText(QApplication::translate("edit_form_skld", "\320\236\321\202\320\274\320\265\320\275\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_form_skld: public Ui_edit_form_skld {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_FORM_SKLD_H
