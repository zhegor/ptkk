#include "edit_platezh.h"
#include "ui_edit_platezh.h"

#include <QSqlQuery>
#include <QTextDocument>
#include <QVariant>
#include <QModelIndex>
#include <QPrinter>
#include <QPrintDialog>
#include <QUrl>
#include <QDesktopServices>

edit_platezh::edit_platezh(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_platezh)
{
    ui->setupUi(this);

    view_platezh = new QSqlRelationalTableModel();
    view_platezh->setTable("platezh_poruch");
    view_platezh->setRelation(3,QSqlRelation("clients","client_id","client_name"));
    view_platezh->select();


    view_platezh->setHeaderData(1,Qt::Horizontal, "�����");
    view_platezh->setHeaderData(2,Qt::Horizontal, "����");
    view_platezh->setHeaderData(3,Qt::Horizontal, "������");

    ui->tableView->setModel(view_platezh);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_platezh::~edit_platezh()
{
    delete ui;
}

void edit_platezh::print_doc()
{
    //������� ���������� � ���
    QSqlQuery getme_ptk_pls;
    getme_ptk_pls.exec("SELECT ptk_name, ptk_inn FROM ptk_info");
    getme_ptk_pls.next();

    QSqlQuery getme_inn_pls;
    getme_inn_pls.exec("SELECT ptk_inn FROM ptk_info");
    getme_inn_pls.next();

    QSqlQuery getme_kpp_pls;
    getme_kpp_pls.exec("SELECT ptk_kpp FROM ptk_info");
    getme_kpp_pls.next();

    QSqlQuery getme_schet_pls;
    getme_schet_pls.exec("SELECT ptk_schet FROM ptk_info");
    getme_schet_pls.next();

    //������� ���������� � ����� ���
    QSqlQuery getme_bankid_pls;
    getme_bankid_pls.exec("SELECT bank_id FROM ptk_info");
    getme_bankid_pls.next();
    //��� �����
    QSqlQuery name_bank_pls;
    name_bank_pls.exec("SELECT bank_name FROM banks WHERE bank_id = " + getme_bankid_pls.value(0).toString() + "");
    name_bank_pls.next();
    //���
    QSqlQuery bik_bank_pls;
    bik_bank_pls.exec("SELECT bank_BIK FROM banks WHERE bank_id = " + getme_bankid_pls.value(0).toString() + "");
    bik_bank_pls.next();
    //�������
    QSqlQuery kor_bank_pls;
    kor_bank_pls.exec("SELECT bank_korschet FROM banks WHERE bank_id = " + getme_bankid_pls.value(0).toString() + "");
    kor_bank_pls.next();

    //������� �������
    QVariant client;
    QModelIndex client_index;
    client_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 3);
    client = ui->tableView->model()->data(client_index);

    QSqlQuery client_inn;
    client_inn.exec("SELECT client_INN FROM clients WHERE client_name ='"+ client.toString() +"'");
    client_inn.next();

    QSqlQuery client_kpp;
    client_kpp.exec("SELECT client_KPP FROM clients WHERE client_name ='" + client.toString()+"'");
    client_kpp.next();

    QSqlQuery client_schet;
    client_schet.exec("SELECT client_schet FROM clients WHERE client_name ='" + client.toString()+"'");
    client_schet.next();

    //������� ���� �������
    QSqlQuery getme_bank2id_pls;
    getme_bank2id_pls.exec("SELECT bank_id FROM clients WHERE client_name ='" + client.toString()+"'");
    getme_bank2id_pls.next();
    //��� �����
    QSqlQuery name_client_pls;
    name_client_pls.exec("SELECT bank_name FROM banks WHERE bank_id = " + getme_bank2id_pls.value(0).toString() + "");
    name_client_pls.next();
    //���
    QSqlQuery bik_client_pls;
    bik_client_pls.exec("SELECT bank_BIK FROM banks WHERE bank_id = " + getme_bank2id_pls.value(0).toString() + "");
    bik_client_pls.next();
    //�������
    QSqlQuery kor_client_pls;
    kor_client_pls.exec("SELECT bank_korschet FROM banks WHERE bank_id = " + getme_bank2id_pls.value(0).toString() + "");
    kor_client_pls.next();


    //////////////////////////////////////////////////
    //����� ��������
    QVariant platezh_nomer;
    QModelIndex platezh_index;
    platezh_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 0);
    platezh_nomer = ui->tableView->model()->data(platezh_index);
    QString actual_nomer_plat = platezh_nomer.toString();

    QVariant platezh_nomer2 = ui->tableView->model()->data(ui->tableView->model()->index(ui->tableView->currentIndex().row(), 1));

    //����
    QVariant data;
    QModelIndex data_index;
    data_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 2);
    data = ui->tableView->model()->data(data_index);
    QString actual_data = data.toString();

    //����������� ����� �� ���� �����
    QSqlQuery summ;
    summ.exec("SELECT sphaktura_tovar_tsena2 FROM sphaktura_tovar WHERE sphaktura_id = (SELECT sphaktura_id FROM sphaktura WHERE platezh_id = " + actual_nomer_plat + ")");
    summ.next();
    QString actual_summ = summ.value(0).toString();

    QTextDocument doc;
    doc.setDefaultFont(QFont("Times", 10, QFont::Normal));
    QString str;

    str += "<html><body>"
           "<h3>��������� ��������� �" + platezh_nomer2.toString() + " �� " + actual_data + "</h3>"
           "<table width = 100% border = 1>"
           "<tr><td>�����" + "<td colspan = 3>" + actual_summ + "  ������"
           "<tr><td colspan =2 >���: " + client_inn.value(0).toString() + "<td colspan = 2>���: " + client_kpp.value(0).toString() + ""
           "<tr><td colspan = 2>" + client.toString() + "<td colspan = 2> ���� �" + client_schet.value(0).toString() +
           "<tr><td colspan = 3>����������"
           "</table><br>"

           "<table width = 100% border = 1>"
           "<tr><td>" + name_client_pls.value(0).toString() + "<td>���:" + bik_client_pls.value(0).toString() + "<td>���� �" + kor_client_pls.value(0).toString() +
           "<tr><td colspan = 3>���� �����������"
           "</table><br>"

           "<table width = 100% border = 1>"
           "<tr><td>" + name_bank_pls.value(0).toString() + "<td>���:" +  bik_bank_pls.value(0).toString() + "<td>���� �" + kor_bank_pls.value(0).toString() +
           "<tr><td colspan = 3>���� ����������"
           "</table><br>"

           "<table width = 100% border = 1>"
           "<tr><td>" + getme_ptk_pls.value(0).toString() + "<td>���: " + getme_inn_pls.value(0).toString() + "<td>���: " + getme_kpp_pls.value(0).toString() +
           "<td>���� �" + getme_schet_pls.value(0).toString() +
           "<tr><td colspan = 4>����������"
           "</table><br><br>"
           "<p>����� - " + actual_summ + " ������</p>"
           "<p>��� - 18%</p><br>"
           "<p>���������� ������� _______________________  (�������)____________________     (������� �����)______________________</p><br>"
           "<p>�.�  __________________ (�������)</p>"

           "</body></html>";

    doc.setHtml(str);

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QCoreApplication::applicationDirPath() + "/docs/platezh/" + platezh_nomer2.toString() + ".pdf");

    doc.print(&printer);
}

void edit_platezh::on_toolButton_clicked()
{
     print_doc();
}

void edit_platezh::on_print_doc_clicked()
{

    QPrinter printer;
       QPrintDialog printDialog(&printer, this);

       if (printDialog.exec())
       {
          print_doc();
       }
}

void edit_platezh::on_show_doc_clicked()
{
    QVariant platezh_nomer;
    QModelIndex platezh_index;
    platezh_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 1);
    platezh_nomer = ui->tableView->model()->data(platezh_index);
    QString actual_nomer_plat = platezh_nomer.toString();

    QDesktopServices::openUrl(QUrl(QCoreApplication::applicationDirPath() + "/docs/platezh/" + actual_nomer_plat + ".pdf"));
}
