#ifndef EDIT_SKLAD_H
#define EDIT_SKLAD_H

#include <QWidget>
#include <QSqlRelationalTableModel>


namespace Ui {
    class edit_sklad;
}

class edit_sklad : public QWidget
{
    Q_OBJECT

public:
    explicit edit_sklad(QWidget *parent = 0);
    ~edit_sklad();

private:
    Ui::edit_sklad *ui;
    QSqlRelationalTableModel *mdl;

};

#endif // EDIT_SKLAD_H
