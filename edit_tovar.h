#ifndef EDIT_TOVAR_H
#define EDIT_TOVAR_H

#include <QWidget>
#include <QSqlRelationalTableModel>

#include "edit_form_tvr.h"

namespace Ui {
    class edit_tovar;
}

/**
  * @class edit_tovar
  * ��������� ���� ����������� �������. ����������� �� QWidget.
*/
class edit_tovar : public QWidget
{
    Q_OBJECT

public:
    explicit edit_tovar(QWidget *parent = 0);
    ~edit_tovar();

private:
    Ui::edit_tovar *ui;
    QSqlRelationalTableModel *mdl;

    edit_form_tvr *edit_form_tvr_win;

private slots:
    void on_comboBox_company_currentIndexChanged(QString );
    void on_tableView_doubleClicked(QModelIndex index);
    void on_delete_2_clicked();
    void on_add_clicked();
};

#endif // EDIT_TOVAR_H
