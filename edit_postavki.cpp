#include "edit_postavki.h"
#include "ui_edit_postavki.h"

#include <QMessageBox>
#include <QSqlQuery>
#include <QDate>
#include <QDesktopServices>
#include <QUrl>


edit_postavki::edit_postavki(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_postavki)
{
    ui->setupUi(this);

    //������� ������
    tovar_mdl = new QSqlRelationalTableModel();
    tovar_mdl->setTable("tovar");
    tovar_mdl->setHeaderData(1,Qt::Horizontal,"���������");
    tovar_mdl->setHeaderData(2,Qt::Horizontal,"�����");
    tovar_mdl->setHeaderData(3,Qt::Horizontal,"�������������");
    tovar_mdl->setHeaderData(4,Qt::Horizontal,"���������");
    tovar_mdl->setHeaderData(5,Qt::Horizontal,"������");
    tovar_mdl->setHeaderData(6,Qt::Horizontal,"��� ������������");

    tovar_mdl->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    tovar_mdl->setRelation(3,QSqlRelation("proizvod","proizvod_id","proizvod_name"));
    tovar_mdl->setRelation(4,QSqlRelation("sost_tovar","sost_id","sost_name"));
    tovar_mdl->setRelation(5,QSqlRelation("strana_pr","strana_id","strana_name"));

    tovar_mdl->setEditStrategy(QSqlTableModel::OnManualSubmit);

    ui->tovarTable->setModel(tovar_mdl);
       tovar_mdl->select();


       QModelIndex categ = ui->comboBox_categorii->model()->index(ui->comboBox_categorii->currentIndex(),1);
         QString categ_str = ui->comboBox_categorii->model()->data(categ).toString();

         tovar_mdl->setFilter("relTblAl_1.category_id = '" + categ_str + "'");



         ui->tovarTable->resizeColumnsToContents();

         ui->tovarTable->hideColumn(0);

         ui->tovarTable->setSelectionBehavior(QAbstractItemView::SelectRows);

         ui->skladView->setSelectionBehavior(QAbstractItemView::SelectRows);


    //������� �����
    sklad = new QSqlRelationalTableModel();
    sklad->setTable("sklad");

    ui->skladView->setModel(sklad);

    sklad->setHeaderData(1,Qt::Horizontal, "���������");
    sklad->setHeaderData(2,Qt::Horizontal, "�����");
    sklad->setHeaderData(3,Qt::Horizontal, "������");
    sklad->setHeaderData(4,Qt::Horizontal, "��� ������������");
    sklad->setHeaderData(5,Qt::Horizontal, "���������");
    sklad->setHeaderData(6,Qt::Horizontal, "���������� (��.)");
    sklad->setHeaderData(7,Qt::Horizontal, "���� ���������� (���.)");
    sklad->setHeaderData(8,Qt::Horizontal, "���� � ������ ������� (���.)");

    sklad->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    sklad->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    sklad->setRelation(3,QSqlRelation("strana_pr","strana_id","strana_name"));
    sklad->setRelation(5,QSqlRelation("postavshiki","post_id","post_name"));
    sklad->select();
    ui->skladView->hideColumn(0);
    ui->skladView->resizeColumnsToContents();



    //������ �����������
    QSqlQueryModel *comb_post = new QSqlQueryModel();
    comb_post->setQuery("SELECT post_name, post_id FROM postavshiki");
    ui->comboBox_post->setModel(comb_post);

    //������ ���������
    QSqlQueryModel *comb_categorii = new QSqlQueryModel();
    comb_categorii->setQuery("SELECT category_name, category_id FROM categorii");
    ui->comboBox_categorii->setModel(comb_categorii);
}

edit_postavki::~edit_postavki()
{
    delete ui;
}

//���������� ������
void edit_postavki::on_add_clicked()
{
    //���� ���� �� ���������, ����� ������
    if(ui->kolvo_line->text().isEmpty() || !ui->tovarTable->currentIndex().isValid())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������, ���������� ������
    else
    {
        QVariant data;
        QModelIndex index;
        index = ui->tovarTable->model()->index(ui->tovarTable->currentIndex().row(), 0, QModelIndex());
        data = ui->tovarTable->model()->data(index);

        //Id ����������
        QModelIndex post_index = ui->comboBox_post->model()->index(ui->comboBox_post->currentIndex(),1);
        QString post_str = ui->comboBox_post->model()->data(post_index).toString();



          //������ �� ������� ������ � ������� id
          QSqlQuery query;
          query.prepare("SELECT predmet_id FROM sklad WHERE tovar_id = '" + data.toString() + "' AND tsena_post = '" +
                        ui->tsena_line->text() +"' AND post_id = '" + post_str + "'");
          query.exec();
          query.next();

          QString tovar_id;
          tovar_id = query.value(0).toString();

          //��������� ��������� ������
          QSqlQuery query_tovar;
          query_tovar.prepare("UPDATE tovar SET sost_id = '1'"
                              "WHERE tovar_id ='" + data.toString() + "'");
          query_tovar.exec();

          //���� ������ ����, ��������� �
          if(!tovar_id.isEmpty())
          {
              int kolvo_1;
              int kolvo_2;

              QSqlQuery query;
              query.prepare("SELECT kol_vo FROM sklad WHERE predmet_id = '" + tovar_id + "'" );
              query.exec();
              query.next();

              kolvo_1 = query.value(0).toInt();
              kolvo_2 = ui->kolvo_line->text().toInt();

              int kolvo_fin = kolvo_1 + kolvo_2;

              QString kolvo_fin_str = QString::number(kolvo_fin);

              QSqlQuery query_add;
              query_add.prepare("UPDATE sklad SET kol_vo = " + kolvo_fin_str +
                                " WHERE predmet_id = " + tovar_id +"");
              query_add.exec();

              sklad->select();
              tovar_mdl->select();

              ui->skladView->resizeColumnsToContents();

          }

          //���� ���, ��������� �����
          else
          {

              //�������� ������ � ������ �� ����


              int row = 0;
              sklad->insertRows(row,1);
              QModelIndex tovar = sklad->index(0,2);
              sklad->setData(tovar,data);

              //Id ���������
              QSqlQuery categery_qr;
              categery_qr.exec("SELECT category_id FROM tovar WHERE tovar_id ='" + data.toString() + "'");
              categery_qr.next();
              QString category_str = categery_qr.value(0).toString();
              QModelIndex category = sklad->index(row,1);
              sklad->setData(category,category_str);

              //Id ������
              QSqlQuery strana_qr;
              strana_qr.exec("SELECT strana_id FROM tovar WHERE tovar_id ='" + data.toString() + "'");
              strana_qr.next();
              QString strana_str = strana_qr.value(0).toString();
              QModelIndex strana = sklad->index(row,3);
              sklad->setData(strana,strana_str);

              //��� ������������
              QSqlQuery god_qr;
              god_qr.exec("SELECT tovar_god FROM tovar WHERE tovar_id ='" + data.toString() + "'");
              god_qr.next();
              QString god_str = god_qr.value(0).toString();
              QModelIndex god = sklad->index(row,4);
              sklad->setData(god,god_str);


              QModelIndex post = sklad->index(row,5);
              sklad->setData(post,post_str);

              //���-��
              QModelIndex kolvo = sklad->index(row,6);
              sklad->setData(kolvo,ui->kolvo_line->text());

              //���� ����������
              QModelIndex tsena = sklad->index(row,7);
              sklad->setData(tsena,ui->tsena_line->text());

              //���� � ��������
              float tsena_post = ui->tsena_line->text().toFloat();

              float prots = ui->natsen_line->text().toFloat() / 100;

              float natsenka = tsena_post * prots;

              float tsena_itog = tsena_post + natsenka;

              QModelIndex tsena_nats = sklad->index(row, 8);
              sklad->setData(tsena_nats,QString::number(tsena_itog));

              sklad->submitAll();

              ui->skladView->resizeColumnsToContents();

              //������� �����
              ui->kolvo_line->setText("");
              ui->tsena_line->setText("");
          }
}
}

//������� � ������� ������
void edit_postavki::on_to_sklad_clicked()
{
    edit_sklad_window = new edit_sklad();
    edit_sklad_window->show();
}

//� ����������
void edit_postavki::on_to_docs_clicked()
{
    QDesktopServices::openUrl(QUrl(QCoreApplication::applicationDirPath() + "/docs/postavki"));
}

//������� ���������
void edit_postavki::on_comboBox_categorii_currentIndexChanged(int index)
{
  QModelIndex categ = ui->comboBox_categorii->model()->index(ui->comboBox_categorii->currentIndex(),1);
    QString categ_str = ui->comboBox_categorii->model()->data(categ).toString();

//    filter = new QSqlQueryModel();
//    filter->setQuery("SELECT * FROM tovar");
//    ui->tovarTable->setModel(filter);
//    filter->setHeaderData(1,Qt::Horizontal,"���������");
//    filter->setHeaderData(2,Qt::Horizontal,"�����");
//    filter->setHeaderData(3,Qt::Horizontal,"�������������");
//    filter->setHeaderData(4,Qt::Horizontal,"���������");
//    filter->setHeaderData(5,Qt::Horizontal,"������");
//    filter->setHeaderData(6,Qt::Horizontal,"��� ������������");
//    ui->tovarTable->hideColumn(0);
////    ui->tovarTable->hideColumn(1);
////    ui->tovarTable->hideColumn(3);
////    ui->tovarTable->hideColumn(4);
////    ui->tovarTable->hideColumn(5);

//    int fuk = 0;

//    for (fuk; fuk > 6; fuk++)
//    {
//        QVariant data;
//        QModelIndex index;
//        index = ui->tovarTable->model()->index(fuk, 1, QModelIndex());
//        data = ui->tovarTable->model()->data(index);

//        if (data == categ_str)
//        {
//            ui->tovarTable->hideRow(fuk);
//        }

//        else
//        {

//        }

    tovar_mdl->setFilter("relTblAl_1.category_id = '" + categ_str + "'");



    ui->tovarTable->resizeColumnsToContents();
}

//�������� ���� �����
void edit_postavki::on_showAll_clicked()
{
    ui->tovarTable->setModel(tovar_mdl);
    tovar_mdl->setFilter("tovar_name IS NOT NULL");
    tovar_mdl->select();
    ui->tovarTable->hideColumn(0);
    ui->tovarTable->showColumn(1);
    ui->tovarTable->showColumn(3);
    ui->tovarTable->showColumn(4);
    ui->tovarTable->showColumn(5);

    ui->tovarTable->resizeColumnsToContents();

}

