#ifndef EDIT_POSTAVKI_H
#define EDIT_POSTAVKI_H

#include <QWidget>
#include <QSqlRelationalTableModel>

#include "edit_sklad.h"

namespace Ui {
    class edit_postavki;
}

class edit_postavki : public QWidget
{
    Q_OBJECT

public:
    explicit edit_postavki(QWidget *parent = 0);
    ~edit_postavki();

private:
    Ui::edit_postavki *ui;
    QSqlRelationalTableModel *tovar_mdl;

    QSqlRelationalTableModel *sklad;

    edit_sklad *edit_sklad_window;


private slots:
    void on_showAll_clicked();
    void on_comboBox_categorii_currentIndexChanged(int index);
    void on_to_docs_clicked();
    void on_to_sklad_clicked();
    void on_add_clicked();
};

#endif // EDIT_POSTAVKI_H
