#ifndef EDIT_PHAKTURA_H
#define EDIT_PHAKTURA_H

#include <QWidget>
#include <QSqlRelationalTableModel>

namespace Ui {
    class edit_phaktura;
}

class edit_phaktura : public QWidget
{
    Q_OBJECT

public:
    explicit edit_phaktura(QWidget *parent = 0);
    ~edit_phaktura();

private:
    Ui::edit_phaktura *ui;

    QSqlRelationalTableModel *view_phaktura;
    QSqlRelationalTableModel *view_ph_tovar;

    QSqlRelationalTableModel *show_phtovar;

    void print_doc();

private slots:
    void on_show_doc_clicked();
    void on_print_doc_clicked();
    void on_save_doc_clicked();
    void on_tableView_clicked(QModelIndex index);
};

#endif // EDIT_PHAKTURA_H
