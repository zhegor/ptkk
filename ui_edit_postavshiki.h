/********************************************************************************
** Form generated from reading UI file 'edit_postavshiki.ui'
**
** Created: Thu 7. May 15:42:52 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_POSTAVSHIKI_H
#define UI_EDIT_POSTAVSHIKI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_postavshiki
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout;
    QPushButton *add;
    QPushButton *delete_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_postavshiki)
    {
        if (edit_postavshiki->objectName().isEmpty())
            edit_postavshiki->setObjectName(QString::fromUtf8("edit_postavshiki"));
        edit_postavshiki->setWindowModality(Qt::ApplicationModal);
        edit_postavshiki->resize(432, 367);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/prk_logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_postavshiki->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_postavshiki);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_postavshiki);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        add = new QPushButton(groupBox);
        add->setObjectName(QString::fromUtf8("add"));

        horizontalLayout->addWidget(add);

        delete_2 = new QPushButton(groupBox);
        delete_2->setObjectName(QString::fromUtf8("delete_2"));

        horizontalLayout->addWidget(delete_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_postavshiki);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_postavshiki);

        QMetaObject::connectSlotsByName(edit_postavshiki);
    } // setupUi

    void retranslateUi(QWidget *edit_postavshiki)
    {
        edit_postavshiki->setWindowTitle(QApplication::translate("edit_postavshiki", "\320\242\320\260\320\261\320\273\320\270\321\206\320\260 \320\277\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272\320\276\320\262", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_postavshiki", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\277\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272\320\260", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_postavshiki", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_postavshiki", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_2->setText(QApplication::translate("edit_postavshiki", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_postavshiki: public Ui_edit_postavshiki {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_POSTAVSHIKI_H
