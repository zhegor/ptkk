#include "edit_phaktura.h"
#include "ui_edit_phaktura.h"


#include <QMessageBox>
#include <QSqlQuery>
#include <QTextDocument>
#include <QPrinter>
#include <QDesktopServices>
#include <QUrl>
#include <QDate>
#include <QPrintDialog>

edit_phaktura::edit_phaktura(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_phaktura)
{
    ui->setupUi(this);

    view_phaktura = new QSqlRelationalTableModel();
    view_phaktura->setTable("sphaktura");

    view_phaktura->setHeaderData(1,Qt::Horizontal,"����� �������");
    view_phaktura->setHeaderData(2,Qt::Horizontal,"����");
    view_phaktura->setHeaderData(3,Qt::Horizontal,"������");
    view_phaktura->setHeaderData(4,Qt::Horizontal,"����� ���������� ���������");
    view_phaktura->setRelation(3,QSqlRelation("clients","client_id","client_name"));
    view_phaktura->setRelation(4,QSqlRelation("platezh_poruch","platezh_id","platezh_nomer"));
    view_phaktura->select();

    ui->tableView->setModel(view_phaktura);
    ui->tableView->hideColumn(0);
    ui->tableView->resizeColumnsToContents();

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_2->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->tableView->hideColumn(5);

}

edit_phaktura::~edit_phaktura()
{
    delete ui;
}

void edit_phaktura::print_doc()
{
    //������� ���������� � ���
    QSqlQuery getme_ptk_pls;
    getme_ptk_pls.exec("SELECT ptk_name FROM ptk_info");
    getme_ptk_pls.next();

    QSqlQuery getme_adres_pls;
    getme_adres_pls.exec("SELECT ptk_adress FROM ptk_info");
    getme_adres_pls.next();

    QSqlQuery getme_inn_pls;
    getme_inn_pls.exec("SELECT ptk_inn FROM ptk_info");
    getme_inn_pls.next();

    QSqlQuery getme_kpp_pls;
    getme_kpp_pls.exec("SELECT ptk_kpp FROM ptk_info");
    getme_kpp_pls.next();

    QSqlQuery getme_schet_pls;
    getme_schet_pls.exec("SELECT ptk_schet FROM ptk_info");
    getme_schet_pls.next();

    ////////////////////////////////////
    //����� �������
    QVariant product_id;
    QModelIndex product_index;
    product_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 1);
    product_id = ui->tableView->model()->data(product_index);
    QString actual_nomer_phak = product_id.toString();

    //����
    QVariant data;
    QModelIndex data_index;
    data_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 2);
    data = ui->tableView->model()->data(data_index);
    QString actual_data = data.toString();

    //������
    QVariant client;
    QModelIndex client_index;
    client_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 3);
    client = ui->tableView->model()->data(client_index);
    QString actual_client = client.toString();


    //����� ����������
    QSqlQuery adress;
    adress.exec("SELECT client_adress FROM clients WHERE client_name = '"+actual_client+"'");
    adress.next();
    QString actual_client_adr = adress.value(0).toString();

    //���
    QSqlQuery inn;
    inn.exec("SELECT client_INN FROM clients WHERE client_name = '"+actual_client+"'");
    inn.next();
    QString actual_client_inn = inn.value(0).toString();

    //���
    QSqlQuery kpp;
    kpp.exec("SELECT client_KPP FROM clients WHERE client_name = '"+actual_client+"'");
    kpp.next();
    QString actual_client_kpp = kpp.value(0).toString();

    //�����
    QVariant actual_tovar;
    QModelIndex index2 = ui->tableView_2->model()->index(0, 2, QModelIndex());
    actual_tovar = ui->tableView_2->model()->data(index2);


    //�����
    QVariant kalvo;
    QModelIndex dfdgh = ui->tableView_2->model()->index(0, 3, QModelIndex());
    kalvo = ui->tableView_2->model()->data(dfdgh);
    QString actual_kolvo = kalvo.toString();


    //����� ����
    QVariant tsen;
    QModelIndex sdfdgh = ui->tableView_2->model()->index(0, 4, QModelIndex());
    tsen = ui->tableView_2->model()->data(sdfdgh);
    QString actual_tsena = tsen.toString();

    float tsenatsena = tsen.toFloat() * kalvo.toFloat();

    QVariant tsen2;
    QModelIndex sdfdgh2 = ui->tableView_2->model()->index(0, 5, QModelIndex());
    tsen2 = ui->tableView_2->model()->data(sdfdgh2);
    QString actual_tsena2 = tsen2.toString();

    float summ_nalog;
    summ_nalog = tsen2.toFloat() * 0.18;

    //������
    QSqlQuery strd;
    strd.exec("SELECT strana_id FROM tovar WHERE tovar_name = '" + actual_tovar.toString() + "'");
    strd.next();
    QString fif = strd.value(0).toString();
    QSqlQuery strd2;
    strd2.exec("SELECT strana_name FROM strana_pr WHERE strana_id =" + fif + "");
    strd2.next();
    QString actual_strana = strd2.value(0).toString();

    //��� ��������
    QTextDocument doc;

    doc.setDefaultFont(QFont("Times", 10, QFont::Normal));
    QString str;
    str += "<html><body>"
           "<h3>����-������� �" + actual_nomer_phak + " �� " + actual_data + "</h3>"
           "<font size = 2>"
           "<table width = 100% border = 0>"
           "<tr><td>��������: " + getme_ptk_pls.value(0).toString() +
           "<tr><td>�����: " + getme_adres_pls.value(0).toString() +
           "<tr><td>���/��� ��������: " + getme_inn_pls.value(0).toString() + " / " + getme_kpp_pls.value(0).toString() +
           "<tr><td>� ���������-���������� ��������� �" + actual_nomer_phak + " �� " + actual_data +
           "<tr><td>����������:" + actual_client +
           "<tr><td>�����:" + actual_client_adr +
           "<tr><td>���/��� ����������:" + actual_client_inn + " / " + actual_client_kpp +
           "</table><br><table width = 100% border = 1 >"
           "<tr><td>������������ ������ <td>������� ��������� <td> ���������� <td>���� �� ������� ��������� <td>��������� ������� ��� ������ - ����� <td> � ��� ����� ����� ������"
           "<td>��������� ������ <td> ����� ������ <td>��������� ������� � ������� - ����� <td> ������ �������������"
           "<tr><td>" + actual_tovar.toString() + "<td>��. <td>" + actual_kolvo + "<td>" + actual_tsena + "<td>" + QString::number(tsenatsena) + "<td>��� ������" +
           "<td>18%" + "<td>" + QString::number(summ_nalog) + "<td>" + tsen2.toString() + "<td>" + actual_strana +
           "</table>"
           "<p>������������ ����������� _________ (�������) ____________ (�.�.�.)</p>"
           "<p>������� ��������� _________ (�������) ____________ (�.�.�.)</p><br>"
           "<p>�������������� ��������������� _________ (�������) ____________ (�.�.�.)</p>"
           "<p>��������� ������������� � ���. ����������� �� _________ (�������) ____________ (�.�.�.)</p><br>"
           "<p>�.�     ____ 20__  ����</p>"
           "</font>"
           "</body></html>"
           "</html>";

    doc.setHtml(str);

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QCoreApplication::applicationDirPath() + "/docs/prodazha/" + actual_nomer_phak + ".pdf");

    doc.print(&printer);

    QSqlQuery query_tovar;
    query_tovar.prepare("UPDATE sphaktura SET doc = '1'"
                        "WHERE sphaktura_nomer = '" + actual_nomer_phak + "'");
    query_tovar.exec();

    view_phaktura->select();

    ui->show_doc->setDisabled(false);



}

void edit_phaktura::on_tableView_clicked(QModelIndex index)
{

    QVariant data;
    QModelIndex index2 = ui->tableView->model()->index(index.row(), 1, QModelIndex());
    data = ui->tableView->model()->data(index2);

    QString fuf = data.toString();

    show_phtovar = new QSqlRelationalTableModel();
    show_phtovar->setTable("sphaktura_tovar");
    ui->tableView_2->setModel(show_phtovar);
    ui->tableView_2->hideColumn(0);
    show_phtovar->setRelation(1,QSqlRelation("sphaktura","sphaktura_id","sphaktura_nomer"));
    show_phtovar->setRelation(2,QSqlRelation("tovar","tovar_id","tovar_name"));
    show_phtovar->setHeaderData(1,Qt::Horizontal,"����� �������");
    show_phtovar->setHeaderData(2,Qt::Horizontal,"�����");
    show_phtovar->setHeaderData(3,Qt::Horizontal,"����������");
    show_phtovar->setHeaderData(4,Qt::Horizontal,"���� �� ������� (���.)");
    show_phtovar->setHeaderData(5,Qt::Horizontal,"����� (���.)");
    show_phtovar->select();

    show_phtovar->setFilter("relTblAl_1.sphaktura_nomer = " + fuf);
    ui->tableView_2->resizeColumnsToContents();

    //��������� ������� ���������
    QVariant doc;
    QModelIndex docindex = ui->tableView->model()->index(index.row(), 5, QModelIndex());
    doc = ui->tableView->model()->data(docindex);

    //�������� ������� ���������
    int docstat = doc.toInt();
    if (docstat == 0)
    {
        ui->show_doc->setDisabled(true);
    }

    else
    {
      ui->show_doc->setDisabled(false);
    }
}





//��������� ��������
void edit_phaktura::on_save_doc_clicked()
{
   print_doc();
}

//���������� ��������
void edit_phaktura::on_print_doc_clicked()
{
    QPrinter printer;
       QPrintDialog printDialog(&printer, this);

       if (printDialog.exec())
       {
          print_doc();
       }
}

//�������� ��������
void edit_phaktura::on_show_doc_clicked()
{
    QVariant product_id;
    QModelIndex product_index;
    product_index = ui->tableView->model()->index(ui->tableView->currentIndex().row(), 1);
    product_id = ui->tableView->model()->data(product_index);
    QString actual_nomer_phak = product_id.toString();

    QDesktopServices::openUrl(QUrl(QCoreApplication::applicationDirPath() +
                                   "/docs/prodazha/" + actual_nomer_phak + ".pdf"));
}

