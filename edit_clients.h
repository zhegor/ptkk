#ifndef EDIT_CLIENTS_H
#define EDIT_CLIENTS_H

#include <QWidget>
#include <QSqlRelationalTableModel>

namespace Ui {
    class edit_clients;
}

class edit_clients : public QWidget
{
    Q_OBJECT

public:
    explicit edit_clients(QWidget *parent = 0);
    ~edit_clients();

    void change_to_postavshiki();
    void change_to_clients();

private:
    Ui::edit_clients *ui;

    QSqlRelationalTableModel *view_clients;

private slots:
    void on_delete_client_clicked();
    void on_add_client_clicked();

};

#endif // EDIT_CLIENTS_H
