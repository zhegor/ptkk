/********************************************************************************
** Form generated from reading UI file 'edit_categorii.ui'
**
** Created: Fri 1. May 15:27:07 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_CATEGORII_H
#define UI_EDIT_CATEGORII_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_categorii
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *category_name_line;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *add_categorii;
    QPushButton *delete_categorii;
    QTableView *tableView;

    void setupUi(QWidget *edit_categorii)
    {
        if (edit_categorii->objectName().isEmpty())
            edit_categorii->setObjectName(QString::fromUtf8("edit_categorii"));
        edit_categorii->setWindowModality(Qt::ApplicationModal);
        edit_categorii->resize(421, 363);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/category.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_categorii->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(edit_categorii);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_categorii);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        category_name_line = new QLineEdit(groupBox);
        category_name_line->setObjectName(QString::fromUtf8("category_name_line"));

        horizontalLayout->addWidget(category_name_line);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        add_categorii = new QPushButton(groupBox);
        add_categorii->setObjectName(QString::fromUtf8("add_categorii"));

        horizontalLayout_2->addWidget(add_categorii);

        delete_categorii = new QPushButton(groupBox);
        delete_categorii->setObjectName(QString::fromUtf8("delete_categorii"));

        horizontalLayout_2->addWidget(delete_categorii);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_categorii);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_categorii);

        QMetaObject::connectSlotsByName(edit_categorii);
    } // setupUi

    void retranslateUi(QWidget *edit_categorii)
    {
        edit_categorii->setWindowTitle(QApplication::translate("edit_categorii", "\320\237\320\265\321\200\320\265\321\207\320\265\320\275\321\214 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\271", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_categorii", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\321\216", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_categorii", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265 \320\272\320\260\321\202\320\265\320\263\320\276\321\200\320\270\320\270", 0, QApplication::UnicodeUTF8));
        add_categorii->setText(QApplication::translate("edit_categorii", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_categorii->setText(QApplication::translate("edit_categorii", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_categorii: public Ui_edit_categorii {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_CATEGORII_H
