#ifndef EDIT_POSTAVSHIKI_H
#define EDIT_POSTAVSHIKI_H

#include <QWidget>
#include <QSqlTableModel>

#include "edit_form.h"

namespace Ui {
    class edit_postavshiki;
}

class edit_postavshiki : public QWidget
{
    Q_OBJECT

public:
    explicit edit_postavshiki(QWidget *parent = 0);
    ~edit_postavshiki();

private:
    Ui::edit_postavshiki *ui;

    QSqlTableModel *mdl;

    edit_form *edit_form_window;

private slots:
    void on_tableView_doubleClicked(QModelIndex index);
    void on_delete_2_clicked();
    void on_add_clicked();
};

#endif // EDIT_POSTAVSHIKI_H
