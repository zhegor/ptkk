/********************************************************************************
** Form generated from reading UI file 'edit_tovar.ui'
**
** Created: Tue 16. Jun 16:50:40 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_TOVAR_H
#define UI_EDIT_TOVAR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_tovar
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBox_category;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QComboBox *comboBox_company;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *lineEdit_name;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QComboBox *comboBox_strana;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *lineEdit_god;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *add;
    QPushButton *delete_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_tovar)
    {
        if (edit_tovar->objectName().isEmpty())
            edit_tovar->setObjectName(QString::fromUtf8("edit_tovar"));
        edit_tovar->setWindowModality(Qt::ApplicationModal);
        edit_tovar->resize(769, 444);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/truck.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_tovar->setWindowIcon(icon);
        edit_tovar->setWindowOpacity(1);
        verticalLayout_2 = new QVBoxLayout(edit_tovar);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(edit_tovar);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("#groupBox{\n"
"	font: 10pt \"MS Shell Dlg 2\";\n"
"}"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        comboBox_category = new QComboBox(groupBox);
        comboBox_category->setObjectName(QString::fromUtf8("comboBox_category"));

        horizontalLayout->addWidget(comboBox_category);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_7->addWidget(label_7);

        comboBox_company = new QComboBox(groupBox);
        comboBox_company->setObjectName(QString::fromUtf8("comboBox_company"));

        horizontalLayout_7->addWidget(comboBox_company);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        lineEdit_name = new QLineEdit(groupBox);
        lineEdit_name->setObjectName(QString::fromUtf8("lineEdit_name"));

        horizontalLayout_2->addWidget(lineEdit_name);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        comboBox_strana = new QComboBox(groupBox);
        comboBox_strana->setObjectName(QString::fromUtf8("comboBox_strana"));

        horizontalLayout_4->addWidget(comboBox_strana);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_5->addWidget(label_5);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        lineEdit_god = new QLineEdit(groupBox);
        lineEdit_god->setObjectName(QString::fromUtf8("lineEdit_god"));

        horizontalLayout_5->addWidget(lineEdit_god);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        add = new QPushButton(groupBox);
        add->setObjectName(QString::fromUtf8("add"));
        add->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_8->addWidget(add);

        delete_2 = new QPushButton(groupBox);
        delete_2->setObjectName(QString::fromUtf8("delete_2"));
        delete_2->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));

        horizontalLayout_8->addWidget(delete_2);


        verticalLayout->addLayout(horizontalLayout_8);


        verticalLayout_2->addWidget(groupBox);

        tableView = new QTableView(edit_tovar);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setSortingEnabled(true);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableView);


        retranslateUi(edit_tovar);

        QMetaObject::connectSlotsByName(edit_tovar);
    } // setupUi

    void retranslateUi(QWidget *edit_tovar)
    {
        edit_tovar->setWindowTitle(QApplication::translate("edit_tovar", "\320\242\320\276\320\262\320\260\321\200", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_tovar", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\267\320\260\320\277\320\270\321\201\321\214", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_tovar", "\320\232\320\260\321\202\320\265\320\263\320\276\321\200\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("edit_tovar", "\320\237\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\321\214", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("edit_tovar", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("edit_tovar", "\320\241\321\202\321\200\320\260\320\275\320\260", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("edit_tovar", "\320\223\320\276\320\264 \320\262\321\213\320\277\321\203\321\201\320\272\320\260", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_tovar", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_2->setText(QApplication::translate("edit_tovar", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_tovar: public Ui_edit_tovar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_TOVAR_H
