#include "edit_postavshiki.h"
#include "ui_edit_postavshiki.h"

#include <QMessageBox>
#include <QDesktopWidget>

edit_postavshiki::edit_postavshiki(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_postavshiki)
{
    ui->setupUi(this);

    //����� �������
    mdl = new QSqlTableModel(this);
    mdl->setTable("postavshiki");
    mdl->select();
    ui->tableView->setModel(mdl);

    mdl->setHeaderData(0,Qt::Horizontal,"ID");
    mdl->setHeaderData(1,Qt::Horizontal,"������������");
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);


    //������� ���� � ����� ������
    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - (this->width()/2));
    center.setY(center.y() - (this->height()/2));
    move(center);

    //����� ��������������
    edit_form_window = new edit_form();
    edit_form_window->setParent(this,Qt::Window);
    edit_form_window->setModel(mdl);
}

edit_postavshiki::~edit_postavshiki()
{
    delete ui;
}

//���������� ������
void edit_postavshiki::on_add_clicked()
{
    //���� �� ��������� ����
    if(ui->lineEdit->text().isEmpty())
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    //���� ���������
    else
    {
      int row = 0;
      mdl->insertRows(row, 1);
      QModelIndex col = mdl->index(row, 1);
      mdl->setData(col, ui->lineEdit->text());
      mdl->submitAll();
      ui->lineEdit->setText("");
      ui->tableView->scrollToBottom();

       }
}


//�������� ������
void edit_postavshiki::on_delete_2_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ������?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    mdl->removeRow(index.row());
}
        }

//����� ��������������
void edit_postavshiki::on_tableView_doubleClicked(QModelIndex index)
{
    edit_form_window->mapper->setCurrentModelIndex(index);
    edit_form_window->show();
}
