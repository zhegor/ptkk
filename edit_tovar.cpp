#include "edit_tovar.h"
#include "ui_edit_tovar.h"

#include <QMessageBox>
#include <QDesktopWidget>


edit_tovar::edit_tovar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::edit_tovar)
{
    ui->setupUi(this);

    //������� ������
    mdl = new QSqlRelationalTableModel();
    mdl->setTable("tovar");
    mdl->setHeaderData(1,Qt::Horizontal,"���������");
    mdl->setHeaderData(2,Qt::Horizontal,"�����");
    mdl->setHeaderData(3,Qt::Horizontal,"�������������");
    mdl->setHeaderData(4,Qt::Horizontal,"���������");
    mdl->setHeaderData(5,Qt::Horizontal,"������");
    mdl->setHeaderData(6,Qt::Horizontal,"��� ������������");

    mdl->setRelation(1,QSqlRelation("categorii","category_id","category_name"));
    mdl->setRelation(3,QSqlRelation("proizvod","proizvod_id","proizvod_name"));
    mdl->setRelation(4,QSqlRelation("sost_tovar","sost_id","sost_name"));
    mdl->setRelation(5,QSqlRelation("strana_pr","strana_id","strana_name"));

    mdl->setEditStrategy(QSqlTableModel::OnManualSubmit);

    mdl->select();

    ui->tableView->setModel(mdl);
    ui->tableView->hideColumn(0);

    //query model - combobox
    QSqlQueryModel *comb_cat = new QSqlQueryModel();
    comb_cat->setQuery("SELECT category_name, category_id FROM categorii");
    ui->comboBox_category->setModel(comb_cat);

    QSqlQueryModel *comb_strana = new QSqlQueryModel();
    comb_strana->setQuery("SELECT strana_name,strana_id FROM strana_pr");
    ui->comboBox_strana->setModel(comb_strana);

    QSqlQueryModel *comb_company = new QSqlQueryModel();
    comb_company->setQuery("SELECT proizvod_name, proizvod_id FROM proizvod");
    ui->comboBox_company->setModel(comb_company);


    //����� ��������������
    edit_form_tvr_win = new edit_form_tvr();
    edit_form_tvr_win->setParent(this,Qt::Window);
    edit_form_tvr_win->setModel(mdl);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

edit_tovar::~edit_tovar()
{
    delete ui;
}

//�������� ������
void edit_tovar::on_add_clicked()
{
    if(ui->lineEdit_name->text().isEmpty() ||
       ui->lineEdit_god->text().isEmpty() )
    {
        QMessageBox::warning(this, "������",
                        "�� �� ��������� ���� ��� ��������� �����");
       }

    else
    {
      int row = 0;
      mdl->insertRows(row, 1);

      QModelIndex col = mdl->index(row, 1);
      QModelIndex index =ui->comboBox_category->model()->index(ui->comboBox_category->currentIndex(),1);
      QString category_id=ui->comboBox_category->model()->data(index).toString();
      mdl->setData(col, category_id);

      QModelIndex col2 = mdl->index(row,2);
      mdl->setData(col2, ui->lineEdit_name->text());

      QModelIndex col3 = mdl->index(row,3);
      QModelIndex index2 =ui->comboBox_company->model()->index(ui->comboBox_company->currentIndex(),1);
      QVariant pr_id=ui->comboBox_company->model()->data(index2);
      mdl->setData(col3, pr_id.toString());

      QModelIndex col4 = mdl->index(row,4);
      mdl->setData(col4, "3");

      QModelIndex col5 = mdl->index(row,5);
      QModelIndex index3 =ui->comboBox_strana->model()->index(ui->comboBox_strana->currentIndex(),1);
      QString strana_id=ui->comboBox_strana->model()->data(index3).toString();
      mdl->setData(col5, strana_id);

      QModelIndex col6 = mdl->index(row,6);
      mdl->setData(col6, ui->lineEdit_god->text());

      mdl->submitAll();

      ui->tableView->scrollToBottom();


       }
}


//�������� ������
void edit_tovar::on_delete_2_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
            if (index.isValid())
            {
                // �������� ������ � ��������
                int ret = QMessageBox::critical(this, "",
                                                tr("�� ������������� ������ ������� ������?"),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No);
                // ���� ����� ������� "��"
                if (ret == QMessageBox::Yes)
                    // �� ������� ������ �� ������
                    mdl->removeRow(index.row());
    }
}

void edit_tovar::on_tableView_doubleClicked(QModelIndex index)
{
    edit_form_tvr_win->mapper->setCurrentModelIndex(index);
    edit_form_tvr_win->show();
}

void edit_tovar::on_comboBox_company_currentIndexChanged(QString )
{
    ui->lineEdit_name->setText(ui->comboBox_company->currentText() + " ");
}
