#ifndef MAINWINDOW_MDI_H
#define MAINWINDOW_MDI_H

#include <QMainWindow>

#include "logindialog.h"
#include "edit_banks.h"
#include "edit_onecolumn.h"
#include "edit_clients.h"
#include "edit_platezh.h"
#include "edit_postavki.h"
#include "edit_proizv.h"
#include "edit_phaktura.h"
#include "edit_sklad.h"
#include "edit_tovar.h"
#include "edit_prod.h"
#include "edit_ptk_info.h"


/**
  * @namespace ������������ ������������ Ui
  * @class ������������ ������� mainwindow_mdi
*/

namespace Ui {
    class mainwindow_mdi;
}


/**
  * @class mainwindow_mdi
  * ��������� ������� ���� ���������. ����������� �� <b>QMainWindow</b>.
���� ����� ��������� ��� ������ ����� ����������, ��� ��� �� ��������� ��������� �������� ���� � ���� ������ �� ���� �������� �������� ���������.
����� mainwindow_mdi ����������� �� QMainWindow � �������� � ���� ��� ��������� ������, ���������� �� ������ �� �������������, ���������� �������� � ������� ������, � ��� �� ������ � �����������.
*/


class mainwindow_mdi : public QMainWindow
{
    Q_OBJECT

public:
    explicit mainwindow_mdi(QWidget *parent = 0);
    ~mainwindow_mdi();

    int user_check(int user_level);

private:

    /**
      *�������������� ����� ��������� ������ ����� mainwindow_mdi.
      */
    Ui::mainwindow_mdi *ui;

    /**
      *���� ����������� ������.
      */
    edit_banks *show_banks;
    /**
      *���� ����������� � �������� ������ ���� ����������.
      */
    edit_onecolumn *show_onecolumn;
    /**
      *���� ����������� ��������.
      */
    edit_clients *show_clients;
    /**
      *���� ����������� ��������� ���������.
      */
    edit_platezh *show_platezh;
    /**
      *���� �������� �������� ������.
      */
    edit_postavki *show_postavki;
    /**
      *���� �������� ������� ������.
      */
    edit_prod *show_prodazha;
    /**
      *���� ����������� ��������������.
      */
    edit_proizv *show_proizv;
    /**
      *���� ����������� ������-������.
      */
    edit_phaktura *show_phaktura;
    /**
      *���� ����������� ������.
      */
    edit_sklad *show_sklad;
    /**
      *���� ����������� �������.
      */
    edit_tovar *show_tovar;
    /**
      *���� � ����������� � �����������.
      */
    edit_ptk_info *show_ptk;

private slots:

    /**
      *���� ���������� �� ����� ����������� ������������
      */
    void on_show_help_triggered();
    /**
      *���� ���������� �� ����� ���� �������������� ���������� � �������� (edit_ptk_info)
      */
    void on_show_company_info_triggered();
    /**
      *���� ���������� �� ����� ������������ �� ������� ������
      */
    void on_logout_triggered();
    /**
      *���� ���������� �� ����� ����  ���������� � ���� �������
      */
    void on_set_tabs_view_triggered();
    /**
      *���� ���������� �� ����� ���� � ���� ����
      */
    void on_set_windows_view_triggered();
    /**
      *���� ���������� �� ����� ���� ����������� ������ (edit_tovar)
      */
    void on_show_tovar_triggered();
    /**
      *���� ���������� �� ����� ���� ����������� ����� (edit_onecolumn)
      */
    void on_show_strana_triggered();
    /**
      *���� ���������� �� ����� ���� ����������� ��������� (edit_onecolumn)
      */
    void on_show_sost_triggered();
    /**
      *���� ���������� �� ����� ���� ������ (edit_sklad)
      */
    void on_show_sklad_triggered();
    /**
      *���� ���������� �� ����� ���� ������-������ (edit_phaktura)
      */
    void on_show_phaktura_triggered();
    /**
      *���� ���������� �� ����� ���� ����������� �������������� (edit_proizv)
      */
    void on_show_proizv_triggered();
    /**
      *���� ���������� �� ����� ���� �������� �������� ������ (edit_postavki)
      */
    void on_show_postavki_triggered();
    /**
      *���� ���������� �� ����� ���� �������� ������� ������ (edit_prod)
      */
    void on_show_prodazha_triggered();
    /**
      *���� ���������� �� ����� ���� ����������� ����������� (edit_clients)
      */
    void on_show_postavshik_triggered();
    /**
      *���� ���������� �� ����� ���� ��������� ��������� (edit_onecolumn)
      */
    void on_show_platezh_triggered();
    /**
      *���� ���������� �� ����� ���� ���������� ��������(edit_clients)
      */
    void on_show_clients_triggered();
    /**
      *���� ���������� �� ����� ���� ���������� ������(edit_banks)
      */
    void on_show_banks_triggered();
    /**
      *���� ���������� �� ����� ���� ���������� ���������(edit_onecolumn)
      */
    void on_show_categorii_triggered();
};

#endif // MAINWINDOW_MDI_H
