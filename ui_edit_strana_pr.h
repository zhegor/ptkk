/********************************************************************************
** Form generated from reading UI file 'edit_strana_pr.ui'
**
** Created: Fri 1. May 17:35:43 2015
**      by: Qt User Interface Compiler version 4.6.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_STRANA_PR_H
#define UI_EDIT_STRANA_PR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_edit_strana_pr
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout;
    QPushButton *add;
    QPushButton *delete_2;
    QTableView *tableView;

    void setupUi(QWidget *edit_strana_pr)
    {
        if (edit_strana_pr->objectName().isEmpty())
            edit_strana_pr->setObjectName(QString::fromUtf8("edit_strana_pr"));
        edit_strana_pr->setWindowModality(Qt::ApplicationModal);
        edit_strana_pr->resize(489, 399);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/strana.png"), QSize(), QIcon::Normal, QIcon::Off);
        edit_strana_pr->setWindowIcon(icon);
        edit_strana_pr->setLayoutDirection(Qt::LeftToRight);
        verticalLayout_3 = new QVBoxLayout(edit_strana_pr);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(edit_strana_pr);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        add = new QPushButton(groupBox);
        add->setObjectName(QString::fromUtf8("add"));

        horizontalLayout->addWidget(add);

        delete_2 = new QPushButton(groupBox);
        delete_2->setObjectName(QString::fromUtf8("delete_2"));

        horizontalLayout->addWidget(delete_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);


        verticalLayout_3->addWidget(groupBox);

        tableView = new QTableView(edit_strana_pr);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_3->addWidget(tableView);


        retranslateUi(edit_strana_pr);

        QMetaObject::connectSlotsByName(edit_strana_pr);
    } // setupUi

    void retranslateUi(QWidget *edit_strana_pr)
    {
        edit_strana_pr->setWindowTitle(QApplication::translate("edit_strana_pr", "\320\241\320\277\321\200\320\260\320\262\320\276\321\207\320\275\320\270\320\272 \321\201\321\202\321\200\320\260\320\275-\320\277\321\200\320\276\320\270\320\267\320\262\320\276\320\264\320\270\321\202\320\265\320\273\320\265\320\271", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("edit_strana_pr", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214 \320\267\320\260\320\277\320\270\321\201\321\214", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("edit_strana_pr", "\320\235\320\260\320\270\320\274\320\265\320\275\320\276\320\262\320\260\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        add->setText(QApplication::translate("edit_strana_pr", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        delete_2->setText(QApplication::translate("edit_strana_pr", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class edit_strana_pr: public Ui_edit_strana_pr {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_STRANA_PR_H
